﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPaging;
using System.Globalization;

namespace inventario.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private InventoryBD db = new InventoryBD();
        private const int defaultPageSize = 10;

        /*CUSTOM*/

        public ViewResultBase Search(string text, string from, string to, int? page)
        {
            IQueryable<Product> productoss = db.Products;

            if (!string.IsNullOrWhiteSpace(from))
            {
                DateTime fromDate = DateTime.Parse(from, CultureInfo.CurrentUICulture);
                productoss = productoss.Where(t => t.TimeStamp >= fromDate);
            }
            if (!string.IsNullOrWhiteSpace(to))
            {
                DateTime toDate = DateTime.Parse(to, CultureInfo.CurrentUICulture);
                productoss = productoss.Where(t => t.TimeStamp <= toDate);
            }

            if (!string.IsNullOrWhiteSpace(text))
            {
                productoss = productoss.Where(t => t.Descripcion.ToLower().IndexOf(text.ToLower()) > -1);
            }

            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var expensesListPaged = productoss.OrderByDescending(i => i.TimeStamp).ToPagedList(currentPageIndex, defaultPageSize);

            if (Request.IsAjaxRequest())
                return PartialView("Index", expensesListPaged);
            else
                return View("Index", expensesListPaged);
        }

        public PartialViewResult RecentProduct(int? top)
        {
            if (!top.HasValue) top = 10;
            var invoices = db.Products.Include(i => i.Provider).OrderByDescending(t=>t.TimeStamp).Take(top.Value);
            return PartialView("ProvedorList", invoices.ToList());
        }

        public PartialViewResult RecentProductByProvider(int? providerID)
        {
            var invoices = db.Products.Include(i => i.Provider).Where(p=>p.ProviderID==providerID).OrderByDescending(t => t.TimeStamp).Take(10);
            return PartialView("ProvedorList", invoices.ToList());
        }
        /*END CUSTOM*/

        //
        // GET: /Producto/

        public ViewResult Index(int? page)
        {
            var product = db.Products.Include(p => p.Provider);
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            return View(product.ToList().ToPagedList(currentPageIndex, defaultPageSize));
        }

        //
        // GET: /Producto/Details/5

        public ViewResult Details(int id)
        {
            Product product = db.Products.Find(id);
            return View(product);
        }

        //
        // GET: /Producto/Create

        public ActionResult Create()
        {
            Product p = new Product();
            p.TimeStamp = DateTime.Now;
            ViewBag.ProviderID = new SelectList(db.Providers, "ProviderID", "Nombre");
            return View(p);
        } 

        //
        // POST: /Producto/Create

        [HttpPost]
        public ActionResult Create(Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.ProviderID = new SelectList(db.Providers, "ProviderID", "Nombre", product.ProviderID);
            return View(product);
        }
        
        //
        // GET: /Producto/Edit/5
 
        public ActionResult Edit(int id)
        {
            Product product = db.Products.Find(id);
            ViewBag.ProviderID = new SelectList(db.Providers, "ProviderID", "Nombre", product.ProviderID);
            return View(product);
        }

        //
        // POST: /Producto/Edit/5

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProviderID = new SelectList(db.Providers, "ProviderID", "Nombre", product.ProviderID);
            return View(product);
        }

        //
        // GET: /Producto/Delete/5
 
        public ActionResult Delete(int id)
        {
            Product product= db.Products.Find(id);
            return View(product);
        }

        //
        // POST: /Producto/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}