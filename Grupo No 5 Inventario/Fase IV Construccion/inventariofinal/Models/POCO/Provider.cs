﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

public class Provider {


    public int ProviderID {get;set;}

    [Required]
    public string Nombre { get; set; }

    [DisplayName("RUC")]
    [Required]
    public string RUC { get; set; }

    [Required]
    public string Direccion { get; set; }

    [Required]
    [DisplayName("Telefono")]
    public string Telefono { get; set; }

    [Required]
    [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Wrong email format")]
    public string Email { get; set; }

    public virtual ICollection<Product> Products { get; set; }
}