﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

public class Inventory {
    [Key]
    public int InventoryID { get; set; }

  
    public string IDArticul { get; set; }
    public virtual Product Product { get; set; }

    public DateTime FechaIngreso { get; set; }

    public int StockMin { get; set; }
    
    public int StockMax { get; set; }

    [Range(0.1, 999999999)]
    public decimal PrecioCompra { get; set; }

    [Range(0.1, 999999999)]
    public decimal PrecioUnitario { get; set; }

    public virtual ICollection<Product> Products { get; set; }
}