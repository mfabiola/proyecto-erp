﻿using System;
using System.Data.Entity;
using System.Collections.Generic;

public class InventoryBD : DbContext {

    public InventoryBD()
    {
        //Set initializer to populate data on database creation
        System.Data.Entity.Database.SetInitializer(new EntitiesContextInitializer());
    }

    public DbSet<Inventory> Inventorys {get; set;}
    public DbSet<Provider> Providers { get; set; }
    public DbSet<Product> Products { get; set; }
    

    
}