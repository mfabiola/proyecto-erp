﻿using System.Data.Entity;
using System.Collections.Generic;
using System;

public class EntitiesContextInitializer : DropCreateDatabaseIfModelChanges<InventoryBD>
{
    protected override void Seed(InventoryBD context)
    {

       


        #region 
        List<Provider> providers=new List<Provider>();
        int providers_count = 40;
        var dummy_provider_names = new string[] { "Hewlett Packard", "Sansum", "Comtech","Toshiba" };
        for (int i = 0; i < providers_count; i++)
        {
            providers.Add(new Provider()
            {
                Nombre = dummy_provider_names[new Random(i).Next(0, dummy_provider_names.Length)] + " " + i,
                Direccion = "Direccion" + i,
                RUC = "212121212" + i,
                Telefono = "2323-2222" + i,
                Email = "sansum_email@provider" + i + ".com"
            });
        }
        foreach (Provider p in providers)
        {
            context.Providers.Add(p);
        }
        #endregion

        #region 
        var articles_dummy = new string[] { "Food expense", "Car expense", "Computer item", "Train ticket", "Plain ticket" };
        for (int m = 1; m < DateTime.Now.Month; m++)
        {
            int expenses_count_per_month = new Random(m).Next(5, 15);
            for (int i = 0; i < expenses_count_per_month; i++)
            {
                context.Products.Add(new Product()
                {
                    Provider = providers[new Random(i).Next(0, providers.Count - 1)],
                    Article = articles_dummy[new Random(i).Next(0, articles_dummy.Length - 1)],
                    Precio = new Random(i).Next(10, 100),
                    TimeStamp = new DateTime (DateTime.Now.Year, m, new Random(i).Next(1, 28))
                });
            }
        }
        #endregion

        
        context.SaveChanges();
    }
}