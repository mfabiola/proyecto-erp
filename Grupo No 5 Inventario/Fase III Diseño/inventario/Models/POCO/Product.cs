﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


public class Product
{
    [Key]
    public int IDArticul { get; set; }

    [Required]
    public string Article {get;set;}

    [Range(0.1, 999999999)]
    public decimal Precio { get; set; }

    public int ProviderID { get; set; }
    public virtual Provider Provider { get; set; }

    public string Descripcion { get; set; }

    public int Existencia { get; set; }

    public string Garantia { get; set; }

    public DateTime TimeStamp { get; set; }


}