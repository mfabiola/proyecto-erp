﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

public class Summary
{
    public DateTime From { get; set; }
    public DateTime To { get; set; }

    public List<Product> Products { get; set; }

    
}