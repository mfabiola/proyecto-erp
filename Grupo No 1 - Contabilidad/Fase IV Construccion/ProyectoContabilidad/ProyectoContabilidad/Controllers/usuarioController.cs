﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class usuarioController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /usuario/

        public ActionResult Index()
        {
            var usuario = db.Usuario.Include("Periodo");
            return View(usuario.ToList());
        }

        //
        // GET: /usuario/Details/5

        public ActionResult Details(string id = null)
        {
            Usuario usuario = db.Usuario.Single(u => u.id_usuario == id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        //
        // GET: /usuario/Create

        public ActionResult Create()
        {
            ViewBag.periodo_actual = new SelectList(db.Periodo, "cod_periodo", "cod_periodo");
            return View();
        }

        //
        // POST: /usuario/Create

        [HttpPost]
        public ActionResult Create(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuario.AddObject(usuario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.periodo_actual = new SelectList(db.Periodo, "cod_periodo", "cod_periodo", usuario.periodo_actual);
            return View(usuario);
        }

        //
        // GET: /usuario/Edit/5

        public ActionResult Edit(string id = null)
        {
            Usuario usuario = db.Usuario.Single(u => u.id_usuario == id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.periodo_actual = new SelectList(db.Periodo, "cod_periodo", "cod_periodo", usuario.periodo_actual);
            return View(usuario);
        }

        //
        // POST: /usuario/Edit/5

        [HttpPost]
        public ActionResult Edit(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuario.Attach(usuario);
                db.ObjectStateManager.ChangeObjectState(usuario, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.periodo_actual = new SelectList(db.Periodo, "cod_periodo", "cod_periodo", usuario.periodo_actual);
            return View(usuario);
        }

        //
        // GET: /usuario/Delete/5

        public ActionResult Delete(string id = null)
        {
            Usuario usuario = db.Usuario.Single(u => u.id_usuario == id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        //
        // POST: /usuario/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            Usuario usuario = db.Usuario.Single(u => u.id_usuario == id);
            db.Usuario.DeleteObject(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}