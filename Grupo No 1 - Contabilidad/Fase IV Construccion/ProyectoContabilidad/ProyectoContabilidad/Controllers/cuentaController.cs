﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class cuentaController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /cuenta/

        public ActionResult Index()
        {
            var cuenta = db.Cuenta.Include("GrupoCuenta").Include("Naturaleza1").Include("TipoCuenta");
            return View(cuenta.ToList());
        }

        //
        // GET: /cuenta/Details/5

        public ActionResult Details(string id = null)
        {
            Cuenta cuenta = db.Cuenta.Single(c => c.num_cuenta == id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        //
        // GET: /cuenta/Create

        public ActionResult Create()
        {
            ViewBag.grupo = new SelectList(db.GrupoCuenta, "num_grupo", "grupo");
            ViewBag.naturaleza = new SelectList(db.Naturaleza, "cod", "nombre");
            ViewBag.tipo = new SelectList(db.TipoCuenta, "num", "tipo");
            return View();
        }

        //
        // POST: /cuenta/Create

        [HttpPost]
        public ActionResult Create(Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Cuenta.AddObject(cuenta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.grupo = new SelectList(db.GrupoCuenta, "num_grupo", "grupo", cuenta.grupo);
            ViewBag.naturaleza = new SelectList(db.Naturaleza, "cod", "nombre", cuenta.naturaleza);
            ViewBag.tipo = new SelectList(db.TipoCuenta, "num", "tipo", cuenta.tipo);
            return View(cuenta);
        }

        //
        // GET: /cuenta/Edit/5

        public ActionResult Edit(string id = null)
        {
            Cuenta cuenta = db.Cuenta.Single(c => c.num_cuenta == id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            ViewBag.grupo = new SelectList(db.GrupoCuenta, "num_grupo", "grupo", cuenta.grupo);
            ViewBag.naturaleza = new SelectList(db.Naturaleza, "cod", "nombre", cuenta.naturaleza);
            ViewBag.tipo = new SelectList(db.TipoCuenta, "num", "tipo", cuenta.tipo);
            return View(cuenta);
        }

        //
        // POST: /cuenta/Edit/5

        [HttpPost]
        public ActionResult Edit(Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Cuenta.Attach(cuenta);
                db.ObjectStateManager.ChangeObjectState(cuenta, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.grupo = new SelectList(db.GrupoCuenta, "num_grupo", "grupo", cuenta.grupo);
            ViewBag.naturaleza = new SelectList(db.Naturaleza, "cod", "nombre", cuenta.naturaleza);
            ViewBag.tipo = new SelectList(db.TipoCuenta, "num", "tipo", cuenta.tipo);
            return View(cuenta);
        }

        //
        // GET: /cuenta/Delete/5

        public ActionResult Delete(string id = null)
        {
            Cuenta cuenta = db.Cuenta.Single(c => c.num_cuenta == id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        //
        // POST: /cuenta/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            Cuenta cuenta = db.Cuenta.Single(c => c.num_cuenta == id);
            db.Cuenta.DeleteObject(cuenta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}