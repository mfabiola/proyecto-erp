﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class grupocuentaController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /grupocuenta/

        public ActionResult Index()
        {
            return View(db.GrupoCuenta.ToList());
        }

        //
        // GET: /grupocuenta/Details/5

        public ActionResult Details(int id = 0)
        {
            GrupoCuenta grupocuenta = db.GrupoCuenta.Single(g => g.num_grupo == id);
            if (grupocuenta == null)
            {
                return HttpNotFound();
            }
            return View(grupocuenta);
        }

        //
        // GET: /grupocuenta/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /grupocuenta/Create

        [HttpPost]
        public ActionResult Create(GrupoCuenta grupocuenta)
        {
            if (ModelState.IsValid)
            {
                db.GrupoCuenta.AddObject(grupocuenta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(grupocuenta);
        }

        //
        // GET: /grupocuenta/Edit/5

        public ActionResult Edit(int id = 0)
        {
            GrupoCuenta grupocuenta = db.GrupoCuenta.Single(g => g.num_grupo == id);
            if (grupocuenta == null)
            {
                return HttpNotFound();
            }
            return View(grupocuenta);
        }

        //
        // POST: /grupocuenta/Edit/5

        [HttpPost]
        public ActionResult Edit(GrupoCuenta grupocuenta)
        {
            if (ModelState.IsValid)
            {
                db.GrupoCuenta.Attach(grupocuenta);
                db.ObjectStateManager.ChangeObjectState(grupocuenta, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(grupocuenta);
        }

        //
        // GET: /grupocuenta/Delete/5

        public ActionResult Delete(int id = 0)
        {
            GrupoCuenta grupocuenta = db.GrupoCuenta.Single(g => g.num_grupo == id);
            if (grupocuenta == null)
            {
                return HttpNotFound();
            }
            return View(grupocuenta);
        }

        //
        // POST: /grupocuenta/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            GrupoCuenta grupocuenta = db.GrupoCuenta.Single(g => g.num_grupo == id);
            db.GrupoCuenta.DeleteObject(grupocuenta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}