﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class asientoController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /asiento/

        public ActionResult Index()
        {
            var asiento = db.Asiento.Include("Usuario");
            return View(asiento.ToList());
        }

        //
        // GET: /asiento/Details/5

        public ActionResult Details(string id = null)
        {
            Asiento asiento = db.Asiento.Single(a => a.num_asiento == id);
            if (asiento == null)
            {
                return HttpNotFound();
            }
            return View(asiento);
        }

        //
        // GET: /asiento/Create

        public ActionResult Create()
        {
            ViewBag.registra = new SelectList(db.Usuario, "id_usuario", "nombre");
            return View();
        }

        //
        // POST: /asiento/Create

        [HttpPost]
        public ActionResult Create(Asiento asiento)
        {
            if (ModelState.IsValid)
            {
                db.Asiento.AddObject(asiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.registra = new SelectList(db.Usuario, "id_usuario", "nombre", asiento.registra);
            return View(asiento);
        }

        //
        // GET: /asiento/Edit/5

        public ActionResult Edit(string id = null)
        {
            Asiento asiento = db.Asiento.Single(a => a.num_asiento == id);
            if (asiento == null)
            {
                return HttpNotFound();
            }
            ViewBag.registra = new SelectList(db.Usuario, "id_usuario", "nombre", asiento.registra);
            return View(asiento);
        }

        //
        // POST: /asiento/Edit/5

        [HttpPost]
        public ActionResult Edit(Asiento asiento)
        {
            if (ModelState.IsValid)
            {
                db.Asiento.Attach(asiento);
                db.ObjectStateManager.ChangeObjectState(asiento, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.registra = new SelectList(db.Usuario, "id_usuario", "nombre", asiento.registra);
            return View(asiento);
        }

        //
        // GET: /asiento/Delete/5

        public ActionResult Delete(string id = null)
        {
            Asiento asiento = db.Asiento.Single(a => a.num_asiento == id);
            if (asiento == null)
            {
                return HttpNotFound();
            }
            return View(asiento);
        }

        //
        // POST: /asiento/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            Asiento asiento = db.Asiento.Single(a => a.num_asiento == id);
            db.Asiento.DeleteObject(asiento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}