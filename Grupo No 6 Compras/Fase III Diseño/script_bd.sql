USE [E:\DOCUMENTS\PROYECTOS\NET\COMPRAS\APP_DATA\COMPRAS.MDF]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 04/28/2013 13:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Producto](
	[idProducto] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[unidadMedida] [nchar](20) NOT NULL,
	[abreviaturaUnidadMedida] [varchar](5) NULL,
	[anulado] [bit] NOT NULL,
 CONSTRAINT [PK1] PRIMARY KEY CLUSTERED 
(
	[idProducto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del producto.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Producto', @level2type=N'COLUMN',@level2name=N'idProducto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la descripción del producto, la cual contiene el nombre u otro detalle que sirve para distinguir el producto.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Producto', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la unidad de medida utilizada para cuatificar el producto.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Producto', @level2type=N'COLUMN',@level2name=N'unidadMedida'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la abreviatura opcional asociada a la unidad de medida en que se cuantifica el producto.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Producto', @level2type=N'COLUMN',@level2name=N'abreviaturaUnidadMedida'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de anulación del producto, si está o no anulado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Producto', @level2type=N'COLUMN',@level2name=N'anulado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa los productos que son solicitados, cotizados y facturados.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Producto'
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 04/28/2013 13:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedor](
	[idProveedor] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[nombreContacto] [varchar](50) NOT NULL,
	[apellidoContacto] [varchar](50) NOT NULL,
	[direccion] [varchar](250) NOT NULL,
	[telefono] [varchar](20) NOT NULL,
	[email] [char](100) NOT NULL,
	[anulado] [bit] NOT NULL,
 CONSTRAINT [PK7] PRIMARY KEY CLUSTERED 
(
	[idProveedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'idProveedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa un descripción breve del proveedor que incluye la razón social de dicho proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el o los nombres de la persona con la cual se contactará al momento de negociar detalles de cotizaciones, ordenes de compras y facturas con el proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'nombreContacto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el o los apellidos de la persona con la cual se contactará al momento de negociar detalles de cotizaciones, ordenes de compras y facturas con el proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'apellidoContacto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la dirección en la cual se encuentra ubicado el proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el teléfono para contactar al proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el e-mail usado como destinatario al enviar correos al proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de anulación del proveedor, si está anulado o no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor', @level2type=N'COLUMN',@level2name=N'anulado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa los proveedores con los cuales se realizan cotizaciones, ordenes de compra de productos y facturaciones de dichas ordenes una vez recibidas.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proveedor'
GO
/****** Object:  Table [dbo].[Ciudad]    Script Date: 04/28/2013 13:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ciudad](
	[idCiudad] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK10] PRIMARY KEY CLUSTERED 
(
	[idCiudad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador único para cada de ciudad.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ciudad', @level2type=N'COLUMN',@level2name=N'idCiudad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el nombre de la ciudad.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ciudad', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa las ciudades disponibles.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ciudad'
GO
SET IDENTITY_INSERT [dbo].[Ciudad] ON
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (1, N'Boaco')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (2, N'Carazo')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (3, N'Chinandega')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (4, N'Chontales')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (5, N'Estelí')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (6, N'Granada')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (7, N'Jinotega')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (8, N'León')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (9, N'Madriz')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (10, N'Managua')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (11, N'Masaya')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (12, N'Matagalpa')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (13, N'Nueva Segovia')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (14, N'Rivas')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (15, N'Río San Juan')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (16, N'Región Autónoma Atlántico Norte')
INSERT [dbo].[Ciudad] ([idCiudad], [descripcion]) VALUES (17, N'Región Autónoma Atlántico Sur')
SET IDENTITY_INSERT [dbo].[Ciudad] OFF
/****** Object:  StoredProcedure [dbo].[CiudadUpdateCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CiudadUpdateCommand]
(
	@descripcion varchar(100),
	@Original_idCiudad bigint,
	@idCiudad bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Ciudad] SET [descripcion] = @descripcion WHERE (([idCiudad] = @Original_idCiudad));
	
SELECT idCiudad, descripcion FROM Ciudad WHERE (idCiudad = @idCiudad)
GO
/****** Object:  StoredProcedure [dbo].[CiudadSelectCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CiudadSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idCiudad, descripcion
FROM            Ciudad
GO
/****** Object:  StoredProcedure [dbo].[CiudadInsertCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CiudadInsertCommand]
(
	@descripcion varchar(100)
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Ciudad] ([descripcion]) VALUES (@descripcion);
	
SELECT idCiudad, descripcion FROM Ciudad WHERE (idCiudad = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[CiudadDeleteCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CiudadDeleteCommand]
(
	@Original_idCiudad bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Ciudad] WHERE (([idCiudad] = @Original_idCiudad))
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado](
	[idEmpleado] [bigint] IDENTITY(1,1) NOT NULL,
	[idCiudad] [bigint] NOT NULL,
	[username] [varchar](20) NOT NULL,
	[cedula] [varchar](20) NOT NULL,
	[nombres] [varchar](50) NOT NULL,
	[apellidos] [varchar](50) NOT NULL,
	[direccion] [varchar](250) NOT NULL,
	[telefono] [varchar](20) NOT NULL,
	[celular] [varchar](20) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[anulado] [bit] NOT NULL,
 CONSTRAINT [PK9] PRIMARY KEY CLUSTERED 
(
	[idEmpleado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [idUserIndex] UNIQUE NONCLUSTERED 
(
	[idEmpleado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [Ref1012] ON [dbo].[Empleado] 
(
	[idCiudad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'idEmpleado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la ciudad a la cual pertenece el empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'idCiudad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el nombre de usuario con el cual se autentifica el empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la cedula que también funciona como identificación para el empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'cedula'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa los nombres del empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'nombres'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa los apellidos del empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'apellidos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la dirección en la cual reside el empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'direccion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el número teléfono de hogar con el cual se puede contactar al empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'telefono'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el número de celular móvil con el cual se puede contactar al empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'celular'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el e-mail utilizado como destinatario para enviarle correos al empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de anulación, si el empleado está o no anulado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado', @level2type=N'COLUMN',@level2name=N'anulado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa los empleados que realizan las solicitudes de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empleado'
GO
/****** Object:  StoredProcedure [dbo].[ProductoUpdateCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductoUpdateCommand]
(
	@descripcion varchar(100),
	@unidadMedida nchar(20),
	@abreviaturaUnidadMedida varchar(5),
	@anulado bit,
	@Original_idProducto bigint,
	@idProducto bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Producto] SET [descripcion] = @descripcion, [unidadMedida] = @unidadMedida, [abreviaturaUnidadMedida] = @abreviaturaUnidadMedida, [anulado] = @anulado WHERE (([idProducto] = @Original_idProducto));
	
SELECT idProducto, descripcion, unidadMedida, abreviaturaUnidadMedida, anulado FROM Producto WHERE (idProducto = @idProducto)
GO
/****** Object:  StoredProcedure [dbo].[ProductoSelectCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductoSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idProducto, descripcion, unidadMedida, abreviaturaUnidadMedida, anulado
FROM            Producto
GO
/****** Object:  StoredProcedure [dbo].[ProductoInsertCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductoInsertCommand]
(
	@descripcion varchar(100),
	@unidadMedida nchar(20),
	@abreviaturaUnidadMedida varchar(5),
	@anulado bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Producto] ([descripcion], [unidadMedida], [abreviaturaUnidadMedida], [anulado]) VALUES (@descripcion, @unidadMedida, @abreviaturaUnidadMedida, @anulado);
	
SELECT idProducto, descripcion, unidadMedida, abreviaturaUnidadMedida, anulado FROM Producto WHERE (idProducto = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[ProductoDeleteCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductoDeleteCommand]
(
	@Original_idProducto bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Producto] WHERE (([idProducto] = @Original_idProducto))
GO
/****** Object:  StoredProcedure [dbo].[ProveedorUpdateCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProveedorUpdateCommand]
(
	@descripcion varchar(100),
	@nombreContacto varchar(50),
	@apellidoContacto varchar(50),
	@direccion varchar(250),
	@telefono varchar(20),
	@email char(100),
	@anulado bit,
	@Original_idProveedor bigint,
	@idProveedor bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Proveedor] SET [descripcion] = @descripcion, [nombreContacto] = @nombreContacto, [apellidoContacto] = @apellidoContacto, [direccion] = @direccion, [telefono] = @telefono, [email] = @email, [anulado] = @anulado WHERE (([idProveedor] = @Original_idProveedor));
	
SELECT idProveedor, descripcion, nombreContacto, apellidoContacto, direccion, telefono, email, anulado FROM Proveedor WHERE (idProveedor = @idProveedor)
GO
/****** Object:  StoredProcedure [dbo].[ProveedorSelectCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProveedorSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idProveedor, descripcion, nombreContacto, apellidoContacto, direccion, telefono, email, anulado
FROM            Proveedor
GO
/****** Object:  StoredProcedure [dbo].[ProveedorInsertCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProveedorInsertCommand]
(
	@descripcion varchar(100),
	@nombreContacto varchar(50),
	@apellidoContacto varchar(50),
	@direccion varchar(250),
	@telefono varchar(20),
	@email char(100),
	@anulado bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Proveedor] ([descripcion], [nombreContacto], [apellidoContacto], [direccion], [telefono], [email], [anulado]) VALUES (@descripcion, @nombreContacto, @apellidoContacto, @direccion, @telefono, @email, @anulado);
	
SELECT idProveedor, descripcion, nombreContacto, apellidoContacto, direccion, telefono, email, anulado FROM Proveedor WHERE (idProveedor = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[ProveedorDeleteCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProveedorDeleteCommand]
(
	@Original_idProveedor bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Proveedor] WHERE (([idProveedor] = @Original_idProveedor))
GO
/****** Object:  Table [dbo].[SolicitudCompra]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolicitudCompra](
	[idSolicitudCompra] [bigint] IDENTITY(1,1) NOT NULL,
	[idEmpleado] [bigint] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[comentarioJefe] [varchar](250) NULL,
	[comentarioMotivo] [varchar](250) NOT NULL,
	[aprobado] [bit] NULL,
	[anulado] [bit] NOT NULL,
 CONSTRAINT [PK2] PRIMARY KEY CLUSTERED 
(
	[idSolicitudCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [Ref92] ON [dbo].[SolicitudCompra] 
(
	[idEmpleado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la solicitud de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra', @level2type=N'COLUMN',@level2name=N'idSolicitudCompra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del empleado que realiza la solicitud de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra', @level2type=N'COLUMN',@level2name=N'idEmpleado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la fecha de creación de la solicitud de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra', @level2type=N'COLUMN',@level2name=N'fechaCreacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el comentario describiendo el motivo de rechazo de solicitud de compra hecho por el jefe, su no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra', @level2type=N'COLUMN',@level2name=N'comentarioJefe'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el comentario describiendo el motivo por el cual el empleado realiza la solicitud de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra', @level2type=N'COLUMN',@level2name=N'comentarioMotivo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de aprobación de la solicitud de compra, si fue aprobada o no por el jefe.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra', @level2type=N'COLUMN',@level2name=N'aprobado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de anulación de la solicitud de compra, si está anulada o no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra', @level2type=N'COLUMN',@level2name=N'anulado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la solicitud de compra de productos hechas por un empleado.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitudCompra'
GO
/****** Object:  StoredProcedure [dbo].[EmpleadoUpdateCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EmpleadoUpdateCommand]
(
	@idCiudad bigint,
	@username varchar(20),
	@cedula varchar(20),
	@nombres varchar(50),
	@apellidos varchar(50),
	@direccion varchar(250),
	@telefono varchar(20),
	@celular varchar(20),
	@email varchar(100),
	@anulado bit,
	@Original_idEmpleado bigint,
	@idEmpleado bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Empleado] SET [idCiudad] = @idCiudad, [username] = @username, [cedula] = @cedula, [nombres] = @nombres, [apellidos] = @apellidos, [direccion] = @direccion, [telefono] = @telefono, [celular] = @celular, [email] = @email, [anulado] = @anulado WHERE (([idEmpleado] = @Original_idEmpleado));
	
SELECT idEmpleado, idCiudad, username, cedula, nombres, apellidos, direccion, telefono, celular, email, anulado FROM Empleado WHERE (idEmpleado = @idEmpleado)
GO
/****** Object:  StoredProcedure [dbo].[EmpleadoSelectCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EmpleadoSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idEmpleado, idCiudad, username, cedula, nombres, apellidos, direccion, telefono, celular, email, anulado
FROM            Empleado
GO
/****** Object:  StoredProcedure [dbo].[EmpleadoInsertCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EmpleadoInsertCommand]
(
	@idCiudad bigint,
	@username varchar(20),
	@cedula varchar(20),
	@nombres varchar(50),
	@apellidos varchar(50),
	@direccion varchar(250),
	@telefono varchar(20),
	@celular varchar(20),
	@email varchar(100),
	@anulado bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Empleado] ([idCiudad], [username], [cedula], [nombres], [apellidos], [direccion], [telefono], [celular], [email], [anulado]) VALUES (@idCiudad, @username, @cedula, @nombres, @apellidos, @direccion, @telefono, @celular, @email, @anulado);
	
SELECT idEmpleado, idCiudad, username, cedula, nombres, apellidos, direccion, telefono, celular, email, anulado FROM Empleado WHERE (idEmpleado = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[EmpleadoDeleteCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EmpleadoDeleteCommand]
(
	@Original_idEmpleado bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Empleado] WHERE (([idEmpleado] = @Original_idEmpleado))
GO
/****** Object:  Table [dbo].[Cotizacion]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cotizacion](
	[idCotizacion] [bigint] IDENTITY(1,1) NOT NULL,
	[idSolicitudCompra] [bigint] NOT NULL,
	[idProveedor] [bigint] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[seleccionada] [bit] NULL,
	[anulado] [bit] NOT NULL,
 CONSTRAINT [PK5] PRIMARY KEY CLUSTERED 
(
	[idCotizacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref25] ON [dbo].[Cotizacion] 
(
	[idSolicitudCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref714] ON [dbo].[Cotizacion] 
(
	[idProveedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador único para cada cotización.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cotizacion', @level2type=N'COLUMN',@level2name=N'idCotizacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la solicitud de compra por la cual se realiza la cotización.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cotizacion', @level2type=N'COLUMN',@level2name=N'idSolicitudCompra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del proveedor con el cual se cotiza.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cotizacion', @level2type=N'COLUMN',@level2name=N'idProveedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la fecha de creación de la cotización.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cotizacion', @level2type=N'COLUMN',@level2name=N'fechaCreacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de selección de la cotización, si fue seleccionada para realizar una orden de compra al proveedor con respecto a la solicitud en cuestión o no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cotizacion', @level2type=N'COLUMN',@level2name=N'seleccionada'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de anulación de la cotización, si está anulada o no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cotizacion', @level2type=N'COLUMN',@level2name=N'anulado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa las cotizaciones hechas a proveedores por razón de una solicitud de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cotizacion'
GO
/****** Object:  Table [dbo].[OrdenCompra]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrdenCompra](
	[idOrdenCompra] [bigint] IDENTITY(1,1) NOT NULL,
	[idSolicitudCompra] [bigint] NOT NULL,
	[idProveedor] [bigint] NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[fechaRecibida] [datetime] NULL,
 CONSTRAINT [PK6] PRIMARY KEY CLUSTERED 
(
	[idOrdenCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref29] ON [dbo].[OrdenCompra] 
(
	[idSolicitudCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref710] ON [dbo].[OrdenCompra] 
(
	[idProveedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la orden de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrdenCompra', @level2type=N'COLUMN',@level2name=N'idOrdenCompra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la solicitud de compra de productos asociada a la orden de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrdenCompra', @level2type=N'COLUMN',@level2name=N'idSolicitudCompra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del proveedor al cual se le ordenan los productos.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrdenCompra', @level2type=N'COLUMN',@level2name=N'idProveedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la fecha de creación de la orden de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrdenCompra', @level2type=N'COLUMN',@level2name=N'fechaCreacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la fecha en que se reciben los productos presentes en el detalle de la solicitud de compra asociada a la orden de compra realizada.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrdenCompra', @level2type=N'COLUMN',@level2name=N'fechaRecibida'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa las ordenes de compra de productos que se envían al proveedor seleccionado luego de las cotizaciones.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrdenCompra'
GO
/****** Object:  Table [dbo].[DetalleSolicitudCompra]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleSolicitudCompra](
	[idDetalleSolicitudCompra] [bigint] IDENTITY(1,1) NOT NULL,
	[idSolicitudCompra] [bigint] NOT NULL,
	[idProducto] [bigint] NOT NULL,
	[precio] [money] NULL,
	[cantidad] [int] NOT NULL,
	[recibido] [bit] NOT NULL,
 CONSTRAINT [PK3] PRIMARY KEY CLUSTERED 
(
	[idDetalleSolicitudCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref14] ON [dbo].[DetalleSolicitudCompra] 
(
	[idProducto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref23] ON [dbo].[DetalleSolicitudCompra] 
(
	[idSolicitudCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del detalle de la solicitud de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleSolicitudCompra', @level2type=N'COLUMN',@level2name=N'idDetalleSolicitudCompra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la solicitud de compra a la cual corresponde el detalle.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleSolicitudCompra', @level2type=N'COLUMN',@level2name=N'idSolicitudCompra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del producto que se solicita comprar.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleSolicitudCompra', @level2type=N'COLUMN',@level2name=N'idProducto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el precio que tiene el producto una vez escogida una cotización' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleSolicitudCompra', @level2type=N'COLUMN',@level2name=N'precio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la cantidad que se necesita comprar del producto.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleSolicitudCompra', @level2type=N'COLUMN',@level2name=N'cantidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado recibido, si la cantidad y el producto específicado fueron recibidos o no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleSolicitudCompra', @level2type=N'COLUMN',@level2name=N'recibido'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa los detalles de las solicitudes de compra.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleSolicitudCompra'
GO
/****** Object:  StoredProcedure [dbo].[SolicitudCompraUpdateCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SolicitudCompraUpdateCommand]
(
	@idEmpleado bigint,
	@fechaCreacion datetime,
	@comentarioJefe varchar(250),
	@comentarioMotivo varchar(250),
	@aprobado bit,
	@anulado bit,
	@Original_idSolicitudCompra bigint,
	@idSolicitudCompra bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [SolicitudCompra] SET [idEmpleado] = @idEmpleado, [fechaCreacion] = @fechaCreacion, [comentarioJefe] = @comentarioJefe, [comentarioMotivo] = @comentarioMotivo, [aprobado] = @aprobado, [anulado] = @anulado WHERE (([idSolicitudCompra] = @Original_idSolicitudCompra));
	
SELECT idSolicitudCompra, idEmpleado, fechaCreacion, comentarioJefe, comentarioMotivo, aprobado, anulado FROM SolicitudCompra WHERE (idSolicitudCompra = @idSolicitudCompra)
GO
/****** Object:  StoredProcedure [dbo].[SolicitudCompraSelectCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SolicitudCompraSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idSolicitudCompra, idEmpleado, fechaCreacion, comentarioJefe, comentarioMotivo, aprobado, anulado
FROM            SolicitudCompra
GO
/****** Object:  StoredProcedure [dbo].[SolicitudCompraInsertCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SolicitudCompraInsertCommand]
(
	@idEmpleado bigint,
	@fechaCreacion datetime,
	@comentarioJefe varchar(250),
	@comentarioMotivo varchar(250),
	@aprobado bit,
	@anulado bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [SolicitudCompra] ([idEmpleado], [fechaCreacion], [comentarioJefe], [comentarioMotivo], [aprobado], [anulado]) VALUES (@idEmpleado, @fechaCreacion, @comentarioJefe, @comentarioMotivo, @aprobado, @anulado);
	
SELECT idSolicitudCompra, idEmpleado, fechaCreacion, comentarioJefe, comentarioMotivo, aprobado, anulado FROM SolicitudCompra WHERE (idSolicitudCompra = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[SolicitudCompraDeleteCommand]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SolicitudCompraDeleteCommand]
(
	@Original_idSolicitudCompra bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [SolicitudCompra] WHERE (([idSolicitudCompra] = @Original_idSolicitudCompra))
GO
/****** Object:  Table [dbo].[Factura]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Factura](
	[idFactura] [bigint] IDENTITY(1,1) NOT NULL,
	[idOrdenCompra] [bigint] NOT NULL,
	[codFactura] [varchar](20) NOT NULL,
	[fechaCreacion] [datetime] NOT NULL,
	[direccionEntrega] [varchar](250) NOT NULL,
	[anulada] [bit] NOT NULL,
 CONSTRAINT [PK8] PRIMARY KEY CLUSTERED 
(
	[idFactura] ASC,
	[idOrdenCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Factura] ON [dbo].[Factura] 
(
	[idFactura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref611] ON [dbo].[Factura] 
(
	[idOrdenCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la factura.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'idFactura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la orden de compra a partir de la cual se generan los detalles de la factura.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'idOrdenCompra'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el código presente en el documento de la factura en físico.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'codFactura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la fecha de creación de la factura.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'fechaCreacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la dirección de entrega en la cual se reciben los productos que se detallan en la orden de compra asociada a la factura.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'direccionEntrega'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el estado de anulación, si la factura está anulada o no.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'anulada'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa las facturas generadas a partir de la información presente en la orden de compra realizada al proveedor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura'
GO
/****** Object:  Table [dbo].[DetalleCotizacion]    Script Date: 04/28/2013 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleCotizacion](
	[idDetalleCotizacion] [bigint] IDENTITY(1,1) NOT NULL,
	[idCotizacion] [bigint] NOT NULL,
	[idProducto] [bigint] NOT NULL,
	[precio] [money] NOT NULL,
	[cantidad] [int] NOT NULL,
 CONSTRAINT [PK4] PRIMARY KEY CLUSTERED 
(
	[idDetalleCotizacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref18] ON [dbo].[DetalleCotizacion] 
(
	[idProducto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref57] ON [dbo].[DetalleCotizacion] 
(
	[idCotizacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del detalle de la cotización.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleCotizacion', @level2type=N'COLUMN',@level2name=N'idDetalleCotizacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador de la cotización a la cual le corresponde el detalle.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleCotizacion', @level2type=N'COLUMN',@level2name=N'idCotizacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el identificador del producto que se desea cotizar' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleCotizacion', @level2type=N'COLUMN',@level2name=N'idProducto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa el precio que se cotizo para el producto asociado al detalle.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleCotizacion', @level2type=N'COLUMN',@level2name=N'precio'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa la cantidad productos que se cotizan.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleCotizacion', @level2type=N'COLUMN',@level2name=N'cantidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Representa los detalles de las cotizaciones.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DetalleCotizacion'
GO
/****** Object:  StoredProcedure [dbo].[CotizacionUpdateCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CotizacionUpdateCommand]
(
	@idSolicitudCompra bigint,
	@idProveedor bigint,
	@fechaCreacion datetime,
	@seleccionada bit,
	@anulado bit,
	@Original_idCotizacion bigint,
	@idCotizacion bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Cotizacion] SET [idSolicitudCompra] = @idSolicitudCompra, [idProveedor] = @idProveedor, [fechaCreacion] = @fechaCreacion, [seleccionada] = @seleccionada, [anulado] = @anulado WHERE (([idCotizacion] = @Original_idCotizacion));
	
SELECT idCotizacion, idSolicitudCompra, idProveedor, fechaCreacion, seleccionada, anulado FROM Cotizacion WHERE (idCotizacion = @idCotizacion)
GO
/****** Object:  StoredProcedure [dbo].[CotizacionSelectCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CotizacionSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idCotizacion, idSolicitudCompra, idProveedor, fechaCreacion, seleccionada, anulado
FROM            Cotizacion
GO
/****** Object:  StoredProcedure [dbo].[CotizacionInsertCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CotizacionInsertCommand]
(
	@idSolicitudCompra bigint,
	@idProveedor bigint,
	@fechaCreacion datetime,
	@seleccionada bit,
	@anulado bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Cotizacion] ([idSolicitudCompra], [idProveedor], [fechaCreacion], [seleccionada], [anulado]) VALUES (@idSolicitudCompra, @idProveedor, @fechaCreacion, @seleccionada, @anulado);
	
SELECT idCotizacion, idSolicitudCompra, idProveedor, fechaCreacion, seleccionada, anulado FROM Cotizacion WHERE (idCotizacion = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[CotizacionDeleteCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CotizacionDeleteCommand]
(
	@Original_idCotizacion bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Cotizacion] WHERE (([idCotizacion] = @Original_idCotizacion))
GO
/****** Object:  StoredProcedure [dbo].[DetalleSolicitudCompraUpdateCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleSolicitudCompraUpdateCommand]
(
	@idSolicitudCompra bigint,
	@idProducto bigint,
	@precio money,
	@cantidad int,
	@recibido bit,
	@Original_idDetalleSolicitudCompra bigint,
	@idDetalleSolicitudCompra bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [DetalleSolicitudCompra] SET [idSolicitudCompra] = @idSolicitudCompra, [idProducto] = @idProducto, [precio] = @precio, [cantidad] = @cantidad, [recibido] = @recibido WHERE (([idDetalleSolicitudCompra] = @Original_idDetalleSolicitudCompra));
	
SELECT idDetalleSolicitudCompra, idSolicitudCompra, idProducto, precio, cantidad, recibido FROM DetalleSolicitudCompra WHERE (idDetalleSolicitudCompra = @idDetalleSolicitudCompra)
GO
/****** Object:  StoredProcedure [dbo].[DetalleSolicitudCompraSelectCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleSolicitudCompraSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idDetalleSolicitudCompra, idSolicitudCompra, idProducto, precio, cantidad, recibido
FROM            DetalleSolicitudCompra
GO
/****** Object:  StoredProcedure [dbo].[DetalleSolicitudCompraInsertCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleSolicitudCompraInsertCommand]
(
	@idSolicitudCompra bigint,
	@idProducto bigint,
	@precio money,
	@cantidad int,
	@recibido bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [DetalleSolicitudCompra] ([idSolicitudCompra], [idProducto], [precio], [cantidad], [recibido]) VALUES (@idSolicitudCompra, @idProducto, @precio, @cantidad, @recibido);
	
SELECT idDetalleSolicitudCompra, idSolicitudCompra, idProducto, precio, cantidad, recibido FROM DetalleSolicitudCompra WHERE (idDetalleSolicitudCompra = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[DetalleSolicitudCompraDeleteCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleSolicitudCompraDeleteCommand]
(
	@Original_idDetalleSolicitudCompra bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [DetalleSolicitudCompra] WHERE (([idDetalleSolicitudCompra] = @Original_idDetalleSolicitudCompra))
GO
/****** Object:  StoredProcedure [dbo].[OrdenCompraUpdateCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdenCompraUpdateCommand]
(
	@idSolicitudCompra bigint,
	@idProveedor bigint,
	@fechaCreacion datetime,
	@fechaRecibida datetime,
	@Original_idOrdenCompra bigint,
	@idOrdenCompra bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [OrdenCompra] SET [idSolicitudCompra] = @idSolicitudCompra, [idProveedor] = @idProveedor, [fechaCreacion] = @fechaCreacion, [fechaRecibida] = @fechaRecibida WHERE (([idOrdenCompra] = @Original_idOrdenCompra));
	
SELECT idOrdenCompra, idSolicitudCompra, idProveedor, fechaCreacion, fechaRecibida FROM OrdenCompra WHERE (idOrdenCompra = @idOrdenCompra)
GO
/****** Object:  StoredProcedure [dbo].[OrdenCompraSelectCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdenCompraSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idOrdenCompra, idSolicitudCompra, idProveedor, fechaCreacion, fechaRecibida
FROM            OrdenCompra
GO
/****** Object:  StoredProcedure [dbo].[OrdenCompraInsertCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdenCompraInsertCommand]
(
	@idSolicitudCompra bigint,
	@idProveedor bigint,
	@fechaCreacion datetime,
	@fechaRecibida datetime
)
AS
	SET NOCOUNT OFF;
INSERT INTO [OrdenCompra] ([idSolicitudCompra], [idProveedor], [fechaCreacion], [fechaRecibida]) VALUES (@idSolicitudCompra, @idProveedor, @fechaCreacion, @fechaRecibida);
	
SELECT idOrdenCompra, idSolicitudCompra, idProveedor, fechaCreacion, fechaRecibida FROM OrdenCompra WHERE (idOrdenCompra = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[OrdenCompraDeleteCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdenCompraDeleteCommand]
(
	@Original_idOrdenCompra bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [OrdenCompra] WHERE (([idOrdenCompra] = @Original_idOrdenCompra))
GO
/****** Object:  StoredProcedure [dbo].[FacturaUpdateCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FacturaUpdateCommand]
(
	@idOrdenCompra bigint,
	@codFactura varchar(20),
	@fechaCreacion datetime,
	@direccionEntrega varchar(250),
	@anulada bit,
	@Original_idFactura bigint,
	@Original_idOrdenCompra bigint,
	@idFactura bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Factura] SET [idOrdenCompra] = @idOrdenCompra, [codFactura] = @codFactura, [fechaCreacion] = @fechaCreacion, [direccionEntrega] = @direccionEntrega, [anulada] = @anulada WHERE (([idFactura] = @Original_idFactura) AND ([idOrdenCompra] = @Original_idOrdenCompra));
	
SELECT idFactura, idOrdenCompra, codFactura, fechaCreacion, direccionEntrega, anulada FROM Factura WHERE (idFactura = @idFactura) AND (idOrdenCompra = @idOrdenCompra)
GO
/****** Object:  StoredProcedure [dbo].[FacturaSelectCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FacturaSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idFactura, idOrdenCompra, codFactura, fechaCreacion, direccionEntrega, anulada
FROM            Factura
GO
/****** Object:  StoredProcedure [dbo].[FacturaInsertCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FacturaInsertCommand]
(
	@idOrdenCompra bigint,
	@codFactura varchar(20),
	@fechaCreacion datetime,
	@direccionEntrega varchar(250),
	@anulada bit
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Factura] ([idOrdenCompra], [codFactura], [fechaCreacion], [direccionEntrega], [anulada]) VALUES (@idOrdenCompra, @codFactura, @fechaCreacion, @direccionEntrega, @anulada);
	
SELECT idFactura, idOrdenCompra, codFactura, fechaCreacion, direccionEntrega, anulada FROM Factura WHERE (idFactura = SCOPE_IDENTITY()) AND (idOrdenCompra = @idOrdenCompra)
GO
/****** Object:  StoredProcedure [dbo].[FacturaDeleteCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FacturaDeleteCommand]
(
	@Original_idFactura bigint,
	@Original_idOrdenCompra bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Factura] WHERE (([idFactura] = @Original_idFactura) AND ([idOrdenCompra] = @Original_idOrdenCompra))
GO
/****** Object:  StoredProcedure [dbo].[DetalleCotizacionUpdateCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleCotizacionUpdateCommand]
(
	@idCotizacion bigint,
	@idProducto bigint,
	@precio money,
	@cantidad int,
	@Original_idDetalleCotizacion bigint,
	@idDetalleCotizacion bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [DetalleCotizacion] SET [idCotizacion] = @idCotizacion, [idProducto] = @idProducto, [precio] = @precio, [cantidad] = @cantidad WHERE (([idDetalleCotizacion] = @Original_idDetalleCotizacion));
	
SELECT idDetalleCotizacion, idCotizacion, idProducto, precio, cantidad FROM DetalleCotizacion WHERE (idDetalleCotizacion = @idDetalleCotizacion)
GO
/****** Object:  StoredProcedure [dbo].[DetalleCotizacionSelectCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleCotizacionSelectCommand]
AS
	SET NOCOUNT ON;
SELECT        idDetalleCotizacion, idCotizacion, idProducto, precio, cantidad
FROM            DetalleCotizacion
GO
/****** Object:  StoredProcedure [dbo].[DetalleCotizacionInsertCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleCotizacionInsertCommand]
(
	@idCotizacion bigint,
	@idProducto bigint,
	@precio money,
	@cantidad int
)
AS
	SET NOCOUNT OFF;
INSERT INTO [DetalleCotizacion] ([idCotizacion], [idProducto], [precio], [cantidad]) VALUES (@idCotizacion, @idProducto, @precio, @cantidad);
	
SELECT idDetalleCotizacion, idCotizacion, idProducto, precio, cantidad FROM DetalleCotizacion WHERE (idDetalleCotizacion = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[DetalleCotizacionDeleteCommand]    Script Date: 04/28/2013 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DetalleCotizacionDeleteCommand]
(
	@Original_idDetalleCotizacion bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [DetalleCotizacion] WHERE (([idDetalleCotizacion] = @Original_idDetalleCotizacion))
GO
/****** Object:  ForeignKey [RefCiudad12]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [RefCiudad12] FOREIGN KEY([idCiudad])
REFERENCES [dbo].[Ciudad] ([idCiudad])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [RefCiudad12]
GO
/****** Object:  ForeignKey [RefEmpleado2]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[SolicitudCompra]  WITH CHECK ADD  CONSTRAINT [RefEmpleado2] FOREIGN KEY([idEmpleado])
REFERENCES [dbo].[Empleado] ([idEmpleado])
GO
ALTER TABLE [dbo].[SolicitudCompra] CHECK CONSTRAINT [RefEmpleado2]
GO
/****** Object:  ForeignKey [RefProveedor14]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[Cotizacion]  WITH CHECK ADD  CONSTRAINT [RefProveedor14] FOREIGN KEY([idProveedor])
REFERENCES [dbo].[Proveedor] ([idProveedor])
GO
ALTER TABLE [dbo].[Cotizacion] CHECK CONSTRAINT [RefProveedor14]
GO
/****** Object:  ForeignKey [RefSolicitudCompra5]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[Cotizacion]  WITH CHECK ADD  CONSTRAINT [RefSolicitudCompra5] FOREIGN KEY([idSolicitudCompra])
REFERENCES [dbo].[SolicitudCompra] ([idSolicitudCompra])
GO
ALTER TABLE [dbo].[Cotizacion] CHECK CONSTRAINT [RefSolicitudCompra5]
GO
/****** Object:  ForeignKey [RefProveedor10]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[OrdenCompra]  WITH CHECK ADD  CONSTRAINT [RefProveedor10] FOREIGN KEY([idProveedor])
REFERENCES [dbo].[Proveedor] ([idProveedor])
GO
ALTER TABLE [dbo].[OrdenCompra] CHECK CONSTRAINT [RefProveedor10]
GO
/****** Object:  ForeignKey [RefSolicitudCompra9]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[OrdenCompra]  WITH CHECK ADD  CONSTRAINT [RefSolicitudCompra9] FOREIGN KEY([idSolicitudCompra])
REFERENCES [dbo].[SolicitudCompra] ([idSolicitudCompra])
GO
ALTER TABLE [dbo].[OrdenCompra] CHECK CONSTRAINT [RefSolicitudCompra9]
GO
/****** Object:  ForeignKey [RefProducto4]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[DetalleSolicitudCompra]  WITH CHECK ADD  CONSTRAINT [RefProducto4] FOREIGN KEY([idProducto])
REFERENCES [dbo].[Producto] ([idProducto])
GO
ALTER TABLE [dbo].[DetalleSolicitudCompra] CHECK CONSTRAINT [RefProducto4]
GO
/****** Object:  ForeignKey [RefSolicitudCompra3]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[DetalleSolicitudCompra]  WITH CHECK ADD  CONSTRAINT [RefSolicitudCompra3] FOREIGN KEY([idSolicitudCompra])
REFERENCES [dbo].[SolicitudCompra] ([idSolicitudCompra])
GO
ALTER TABLE [dbo].[DetalleSolicitudCompra] CHECK CONSTRAINT [RefSolicitudCompra3]
GO
/****** Object:  ForeignKey [RefOrdenCompra11]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [RefOrdenCompra11] FOREIGN KEY([idOrdenCompra])
REFERENCES [dbo].[OrdenCompra] ([idOrdenCompra])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [RefOrdenCompra11]
GO
/****** Object:  ForeignKey [RefCotizacion7]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[DetalleCotizacion]  WITH CHECK ADD  CONSTRAINT [RefCotizacion7] FOREIGN KEY([idCotizacion])
REFERENCES [dbo].[Cotizacion] ([idCotizacion])
GO
ALTER TABLE [dbo].[DetalleCotizacion] CHECK CONSTRAINT [RefCotizacion7]
GO
/****** Object:  ForeignKey [RefProducto8]    Script Date: 04/28/2013 13:02:21 ******/
ALTER TABLE [dbo].[DetalleCotizacion]  WITH CHECK ADD  CONSTRAINT [RefProducto8] FOREIGN KEY([idProducto])
REFERENCES [dbo].[Producto] ([idProducto])
GO
ALTER TABLE [dbo].[DetalleCotizacion] CHECK CONSTRAINT [RefProducto8]
GO
