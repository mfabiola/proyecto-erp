﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaDeSolicitudesParaAprobar.aspx.cs" Inherits="Compras.User.Jefe.ListaDeSolicitudesParaAprobar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Lista de solicitudes de compra para aprobar</h2>
<br />
    <asp:EntityDataSource ID="SolicitudCompraDataSource" runat="server" 
        ContextTypeName="Compras.DAL.ComprasEntities" 
        EnableDelete="True" EnableFlattening="False" EnableUpdate="True" 
        EntitySetName="SolicitudCompra" Where="it.anulado = FALSE AND it.aprobado is null">
    </asp:EntityDataSource>
    <asp:GridView ID="SolicitudCompraGridView" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" 
        DataKeyNames="idSolicitudCompra" DataSourceID="SolicitudCompraDataSource"
        OnRowCommand="SolicitudCompraGridView_RowCommand" CssClass="table table-striped">
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="idSolicitudCompra" HeaderText="ID Solicitud Compra" 
                SortExpression="idSolicitudCompra" ReadOnly="True" />
            <asp:BoundField DataField="idEmpleado" HeaderText="ID Empleado" 
                SortExpression="idEmpleado" ReadOnly="True" />
            <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha" 
                SortExpression="fechaCreacion" ReadOnly="True" />
            <asp:BoundField DataField="comentarioMotivo" HeaderText="Comentario Empleado" 
                SortExpression="comentarioMotivo" ReadOnly="True" />             
            <asp:BoundField DataField="comentarioJefe" HeaderText="Comentario Jefe" 
                SortExpression="comentarioJefe" />
            <asp:ButtonField ButtonType="Link" CommandName="Aprobar" Text="Aprobar" />
            <asp:ButtonField ButtonType="Link" CommandName="Rechazar" Text="Rechazar" />
        </Columns>
    </asp:GridView>
</asp:Content>
