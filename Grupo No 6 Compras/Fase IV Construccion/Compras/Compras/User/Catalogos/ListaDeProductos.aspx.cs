﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Compras.DAL;

namespace Compras.User.Catalogos
{
    public partial class ListaDeProductos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ProductoGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "Anular":
                    var id1 = Convert.ToInt64(ProductoGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text);

                    using (var context = new ComprasEntities())
                    {
                        Producto pd = context.Producto.First(i => i.idProducto == id1);
                        pd.anulado = true;
                        context.SaveChanges();
                    }
                    break;
            }

            ProductoGridView.DataBind();

        }

    }
}