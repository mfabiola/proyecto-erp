﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AgregarProveedor.aspx.cs" Inherits="Compras.User.Catalogos.AgregarProveedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Agregar Proveedor</h2>
    <asp:EntityDataSource ID="ProveedorEntityDataSource" runat="server"
        ContextTypeName="Compras.DAL.ComprasEntities" EnableFlattening="False"
        EnableInsert="True" EntitySetName="Proveedor">
    </asp:EntityDataSource>
    <asp:DetailsView ID="ProveedorDetailsView" runat="server" 
        DataSourceID="ProveedorEntityDataSource" AutoGenerateRows="False"
        DefaultMode="Insert" CssClass="table">
        <Fields>
            <asp:BoundField DataField="idProveedor" HeaderText="idProveedor" 
                SortExpression="idProveedor" />
            <asp:BoundField DataField="descripcion" HeaderText="descripcion" 
                SortExpression="descripcion" />
            <asp:BoundField DataField="nombreContacto" HeaderText="nombreContacto" 
                SortExpression="nombreContacto" />
            <asp:BoundField DataField="apellidoContacto" HeaderText="apellidoContacto" 
                SortExpression="apellidoContacto" />
            <asp:BoundField DataField="direccion" HeaderText="direccion" 
                SortExpression="direccion" />
            <asp:BoundField DataField="telefono" HeaderText="telefono" 
                SortExpression="telefono" />
            <asp:BoundField DataField="email" HeaderText="email" 
                SortExpression="email" />
             <asp:CommandField ShowInsertButton="True" />
       </Fields>
    </asp:DetailsView>

</asp:Content>
