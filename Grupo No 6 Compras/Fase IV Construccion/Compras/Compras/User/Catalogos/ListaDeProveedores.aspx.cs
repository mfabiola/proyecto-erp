﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Compras.DAL;

namespace Compras.User.Catalogos
{
    public partial class ListaDeProveedores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ProveedorGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "Anular":
                    var id1 = Convert.ToInt64(ProveedorGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text);

                    using (var context = new ComprasEntities())
                    {
                        Proveedor pv = context.Proveedor.First(i => i.idProveedor == id1);
                        pv.anulado = true;
                        context.SaveChanges();
                    }
                    break;
            }

            ProveedorGridView.DataBind();

        }

    }
}