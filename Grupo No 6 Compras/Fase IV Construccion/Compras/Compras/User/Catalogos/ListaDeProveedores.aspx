﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaDeProveedores.aspx.cs" Inherits="Compras.User.Catalogos.ListaDeProveedores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Lista de Proveedores</h2>
<br />
    <asp:EntityDataSource ID="ProveedorDataSource" runat="server" 
        ContextTypeName="Compras.DAL.ComprasEntities" 
        EnableDelete="True" EnableFlattening="False" EnableUpdate="True" 
        EntitySetName="Proveedor" Where="it.anulado = FALSE">
    </asp:EntityDataSource>
    <asp:GridView ID="ProveedorGridView" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" 
        DataKeyNames="idProveedor" DataSourceID="ProveedorDataSource"
        OnRowCommand="ProveedorGridView_RowCommand" CssClass="table table-striped">
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="idProveedor" HeaderText="ID Proveedor" 
                SortExpression="idProveedor" ReadOnly="True" />
            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" 
                SortExpression="descripcion" />
            <asp:BoundField DataField="nombreContacto" HeaderText="Nombre" 
                SortExpression="nombreContacto" />
            <asp:BoundField DataField="apellidoContacto" HeaderText="Apellido" 
                SortExpression="apellidoContacto" />
            <asp:BoundField DataField="direccion" HeaderText="Direccion" 
                SortExpression="direccion" />
            <asp:BoundField DataField="telefono" HeaderText="Telefono" 
                SortExpression="telefono" />
            <asp:BoundField DataField="email" HeaderText="Email" 
                SortExpression="email" />
            <asp:ButtonField ButtonType="Link" CommandName="Anular" Text="Anular" />
        </Columns>
    </asp:GridView>
</asp:Content>