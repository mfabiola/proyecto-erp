﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Compras.DAL;

namespace Compras.User.Empleado
{
    public partial class ListaDeSolicitudesPendientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("Loading componentosu desu...");

        }


        protected void SolicitudCompraGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            switch(e.CommandName)
            {
                case "Anular":
                    var id1 = Convert.ToInt64(SolicitudCompraGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text);

                    using (var context = new ComprasEntities())
                    {
                        SolicitudCompra sc = context.SolicitudCompra.First(i => i.idSolicitudCompra == id1);
                        sc.anulado = true;
                        context.SaveChanges();
                    }
                    break;
            }
            
        SolicitudCompraGridView.DataBind();

        }
    }
}