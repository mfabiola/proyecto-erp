﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AgregarSolicitud.aspx.cs" Inherits="Compras.User.Compras.AgregarSolicitud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Agregar Solicitud de Compra</h2>
    <asp:EntityDataSource ID="SolicitudCompraEntityDataSource" runat="server"
        ContextTypeName="Compras.DAL.ComprasEntities" EnableFlattening="False"
        EnableInsert="True" EntitySetName="SolicitudCompra">
    </asp:EntityDataSource>
    <asp:DetailsView ID="SolicitudCompraDetailsView" runat="server" 
        DataSourceID="SolicitudCompraEntityDataSource" AutoGenerateRows="False"
        DefaultMode="Insert" CssClass="table">
        <Fields>
            <asp:BoundField DataField="idSolicitudCompra" HeaderText="idSolicitudCompra" 
                SortExpression="idSolicitudCompra" />
            <asp:BoundField DataField="idEmpleado" HeaderText="idEmpleado" 
                SortExpression="idEmpleado" />
            <asp:BoundField DataField="comentarioMotivo" HeaderText="comentarioMotivo" 
                SortExpression="comentarioMotivo" />
            <asp:BoundField DataField="fechaCreacion" HeaderText="fechaCreacion" 
                SortExpression="fechaCreacion" />
             <asp:CommandField ShowInsertButton="True" />
       </Fields>
    </asp:DetailsView>
</asp:Content>
