﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaDeSolicitudesRealizadas.aspx.cs" Inherits="Compras.User.Empleado.ListaDeSolicitudesPendientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Lista de solicitudes de compra realizadas</h2>
<br />
    <asp:EntityDataSource ID="SolicitudCompraDataSource" runat="server" 
        ContextTypeName="Compras.DAL.ComprasEntities" 
        EnableDelete="True" EnableFlattening="False" EnableUpdate="True" 
        EntitySetName="SolicitudCompra" Where="it.anulado = FALSE AND it.aprobado is null">
    </asp:EntityDataSource>
    <asp:GridView ID="SolicitudCompraGridView" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" 
        DataKeyNames="idSolicitudCompra" DataSourceID="SolicitudCompraDataSource"
        OnRowCommand="SolicitudCompraGridView_RowCommand" CssClass="table table-striped">
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="idSolicitudCompra" HeaderText="ID Solicitud Compra" 
                SortExpression="idSolicitudCompra" />
            <asp:BoundField DataField="idEmpleado" HeaderText="ID Empleado" 
                SortExpression="idEmpleado" />
            <asp:BoundField DataField="comentarioMotivo" HeaderText="Comentario Motivo" 
                SortExpression="comentarioMotivo" />
            <asp:BoundField DataField="fechaCreacion" HeaderText="Fecha" 
                SortExpression="fechaCreacion" />
            <asp:ButtonField ButtonType="Link" CommandName="Anular" Text="Anular" />
        </Columns>
    </asp:GridView>
</asp:Content>
