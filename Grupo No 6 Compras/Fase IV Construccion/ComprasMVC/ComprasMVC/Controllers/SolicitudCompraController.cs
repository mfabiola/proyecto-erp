﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ComprasMVC.Models;

namespace ComprasMVC.Controllers
{
    public class SolicitudCompraController : Controller
    {
        //
        // GET: /SolicitudCompra/

        public ActionResult Index()
        {
            var db = new ComprasEntities1();
            return View(db.SolicitudCompra.ToList());
        }

        //
        // GET: /SolicitudCompra/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /SolicitudCompra/Create

        public ActionResult Create()
        {
            var solicitudCompra = new SolicitudCompra();
            solicitudCompra.idSolicitudCompra = 0;
            return View(solicitudCompra);
        }

        //
        // POST: /SolicitudCompra/Create

        [HttpPost]
        public ActionResult Create(SolicitudCompra newSolicitudCompra)
        {
            try
            {
                using (var db = new ComprasEntities1())
                {
                    newSolicitudCompra.fechaCreacion = DateTime.Now;
                    db.SolicitudCompra.AddObject(newSolicitudCompra);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SolicitudCompra/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /SolicitudCompra/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SolicitudCompra/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /SolicitudCompra/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
