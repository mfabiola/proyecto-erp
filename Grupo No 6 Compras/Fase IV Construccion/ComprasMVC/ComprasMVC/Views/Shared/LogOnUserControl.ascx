﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul class="nav pull-right">
    <li class="dropdown pull-right"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
        Cuenta <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <%
                if (Request.IsAuthenticated)
                {
            %><li>Bienvenido <b>
                <%: Page.User.Identity.Name %></b>!
                <%: Html.ActionLink("Cerrar sesión", "LogOff", "Account")%></li>
            <%
                }
                else
                {
            %>
            <li>
                <%: Html.ActionLink("Iniciar sesión", "LogOn", "Account")%></li>
            <li>
                <%: Html.ActionLink("Registrarse", "Register", "Account")%></li>
            <%
                }
            %>
        </ul>
    </li>
</ul>
