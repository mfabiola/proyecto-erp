﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<ComprasMVC.Models.SolicitudCompra>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Listado de solicitudes de compra
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Listado de solicitudes de compra</h2>

    <table class="table">
        <tr>
            <th></th>
            <th>
                Id
            </th>
            <th>
                Empleado
            </th>
            <th>
                Fecha de creación
            </th>
            <th>
                Comentario del motivo
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    
        <tr>
            <td>
                <%: Html.ActionLink("Modificar", "Edit", new { id=item.idSolicitudCompra }) %> |
                <%: Html.ActionLink("Visualizar", "Details", new { id=item.idSolicitudCompra })%> |
                <%: Html.ActionLink("Anular", "Delete", new { id=item.idSolicitudCompra })%>
            </td>
            <td>
                <%: item.idSolicitudCompra %>
            </td>
            <td>
                <%: item.Empleado.nombres + item.Empleado.apellidos %>
            </td>
            <td>
                <%: String.Format("{0:g}", item.fechaCreacion) %>
            </td>
            <td>
                <%: item.comentarioMotivo %>
            </td>
        </tr>
    
    <% } %>

    </table>

    <p>
        <%: Html.ActionLink("Registrar nueva", "Create") %>
    </p>

</asp:Content>

