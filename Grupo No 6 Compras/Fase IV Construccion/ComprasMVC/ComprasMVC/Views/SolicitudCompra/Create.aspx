﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ComprasMVC.Models.SolicitudCompra>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Registrar solicitud de compra
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Registrar solicitud de compra</h2>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset>
            <legend>Fields</legend>
            
            <div class="editor-field">
                <%: Html.HiddenFor(model => model.idSolicitudCompra) %>
                <%: Html.ValidationMessageFor(model => model.idSolicitudCompra) %>
            </div>
            
            <div class="editor-label">
                <label for="comentarioMotivo">Motivo</label>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.comentarioMotivo, new { placeholder = "Motivo" })%>
                <%: Html.ValidationMessageFor(model => model.comentarioMotivo) %>
            </div>
            
            <p>
                <input type="submit" value="Registrar" />
            </p>
        </fieldset>

    <% } %>

    <div>
        <%: Html.ActionLink("Volver al listado", "Index") %>
    </div>

</asp:Content>

