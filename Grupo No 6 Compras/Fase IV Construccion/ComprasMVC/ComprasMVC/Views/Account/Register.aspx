﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ComprasMVC.Models.RegisterModel>" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Registrarse
</asp:Content>
<asp:Content ID="registerContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-signin">
        <h2 class="form-signin-heading">
            Registro</h2>
        <p>
            Las contraseñas requieren una longitud mínima de
            <%: ViewData["PasswordLength"] %>
            caracteres.
        </p>
        <% using (Html.BeginForm())
           { %>
        <%: Html.ValidationSummary(true, "El registro no tuvó éxito. Por favor corrija los errores e intente nuevamente.")%>
        <div>
            <div class="editor-label">
                <%: Html.LabelFor(m => m.UserName) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(m => m.UserName, new { placeholder = "Usuario", @class = "input-block-level" })%>
                <%: Html.ValidationMessageFor(m => m.UserName) %>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(m => m.Email) %>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(m => m.Email, new { placeholder = "E-mail", @class = "input-block-level" })%>
                <%: Html.ValidationMessageFor(m => m.Email) %>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(m => m.Password) %>
            </div>
            <div class="editor-field">
                <%: Html.PasswordFor(m => m.Password, new { placeholder = "Contraseña", @class = "input-block-level" })%>
                <%: Html.ValidationMessageFor(m => m.Password) %>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(m => m.ConfirmPassword) %>
            </div>
            <div class="editor-field">
                <%: Html.PasswordFor(m => m.ConfirmPassword, new { placeholder = "Repetir contraseña", @class = "input-block-level" })%>
                <%: Html.ValidationMessageFor(m => m.ConfirmPassword) %>
            </div>
            <p>
                <input class="btn btn-large btn-primary" type="submit" value="Registrarse" />
            </p>
        </div>
        <% } %>
</asp:Content>
