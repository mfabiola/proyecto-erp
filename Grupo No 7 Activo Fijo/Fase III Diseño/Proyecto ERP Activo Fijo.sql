/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      Ebisoft
 * Project :      Proyecto Activo Fijo.dm1
 * Author :       Ronnie Rios
 *
 * Date Created : Friday, May 03, 2013 20:07:34
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE [Activo Fijo]
go
USE [Activo Fijo]
go
/* 
 * TABLE: Activos 
 */

CREATE TABLE Activos(
    Codigo          char(10)       NOT NULL,
    Fecha_Compra    date           NULL,
    Nombre          varchar(30)    NULL,
    Color           char(10)       NULL,
    Marca           char(10)       NULL,
    Modelo          char(15)       NULL,
    Serie           char(15)       NULL,
    CONSTRAINT PK4 PRIMARY KEY CLUSTERED (Codigo)
)
go



IF OBJECT_ID('Activos') IS NOT NULL
    PRINT '<<< CREATED TABLE Activos >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Activos >>>'
go

/* 
 * TABLE: Altas 
 */

CREATE TABLE Altas(
    Codigo                       int            NOT NULL,
    Codigo_TP                    int            NOT NULL,
    Codigo_Proveedores           int            NOT NULL,
    Codigo_Responsable           int            NOT NULL,
    Codigo_TI                    int            NOT NULL,
    Codigo_Estado                char(10)       NOT NULL,
    Codigo_activo                char(10)       NOT NULL,
    [Codigo_Ubicacion']          int            NOT NULL,
    Valor_Rescate                float          NULL,
    Observacion                  varchar(50)    NULL,
    Fecha_Alta                   date           NULL,
    Numero_Factura               int            NULL,
    Fecha_Inicio_Depreciacion    date           NULL,
    [Vida Util]                  int            NULL,
    Valor_Compra                 float          NULL,
    Valor_depreciacion           float          NULL,
    [% anual]                    float          NULL,
    Codigo_Proveedor             int            NULL,
    CONSTRAINT PK6 PRIMARY KEY CLUSTERED (Codigo, Codigo_TP, Codigo_Proveedores, Codigo_Responsable, Codigo_TI, Codigo_Estado, Codigo_activo, [Codigo_Ubicacion'])
)
go



IF OBJECT_ID('Altas') IS NOT NULL
    PRINT '<<< CREATED TABLE Altas >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Altas >>>'
go

/* 
 * TABLE: Bajas 
 */

CREATE TABLE Bajas(
    Codigo                int            IDENTITY(1,1),
    [Tipo de baja]        int            NOT NULL,
    Codigo_ubicacion      int            NOT NULL,
    Codigo_responsable    int            NOT NULL,
    Codigo_activo         char(10)       NOT NULL,
    Fecha_Baja            date           NULL,
    Observacion           varchar(30)    NULL,
    CONSTRAINT PK7 PRIMARY KEY CLUSTERED (Codigo, [Tipo de baja], Codigo_ubicacion, Codigo_responsable, Codigo_activo)
)
go



IF OBJECT_ID('Bajas') IS NOT NULL
    PRINT '<<< CREATED TABLE Bajas >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Bajas >>>'
go

/* 
 * TABLE: Depreciaciones 
 */

CREATE TABLE Depreciaciones(
    Codigo                  int         NOT NULL,
    Codigo_activo           char(10)    NOT NULL,
    Depreciacion_Mensual    float       NULL,
    Depreciacion_Diaria     float       NULL,
    Fecha_Depreciacion      date        NULL,
    Depreciacion_Anual      float       NULL,
    CONSTRAINT PK16 PRIMARY KEY CLUSTERED (Codigo, Codigo_activo)
)
go



IF OBJECT_ID('Depreciaciones') IS NOT NULL
    PRINT '<<< CREATED TABLE Depreciaciones >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Depreciaciones >>>'
go

/* 
 * TABLE: Empleados 
 */

CREATE TABLE Empleados(
    Codigo             int            IDENTITY(1,1),
    [Nombre Completo]  varchar(40)    NOT NULL,
    CONSTRAINT PK9 PRIMARY KEY CLUSTERED (Codigo)
)
go



IF OBJECT_ID('Empleados') IS NOT NULL
    PRINT '<<< CREATED TABLE Empleados >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Empleados >>>'
go

/* 
 * TABLE: Estado 
 */

CREATE TABLE Estado(
    Codigo_Estado    char(10)       NOT NULL,
    Descripcion      varchar(20)    NOT NULL,
    CONSTRAINT PK11 PRIMARY KEY CLUSTERED (Codigo_Estado)
)
go



IF OBJECT_ID('Estado') IS NOT NULL
    PRINT '<<< CREATED TABLE Estado >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Estado >>>'
go

/* 
 * TABLE: Proveedores 
 */

CREATE TABLE Proveedores(
    Codigo        int            IDENTITY(1,1),
    Dscripcion    varchar(30)    NOT NULL,
    CONSTRAINT PK12 PRIMARY KEY CLUSTERED (Codigo)
)
go



IF OBJECT_ID('Proveedores') IS NOT NULL
    PRINT '<<< CREATED TABLE Proveedores >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Proveedores >>>'
go

/* 
 * TABLE: [Tipo de activos] 
 */

CREATE TABLE [Tipo de activos](
    Codigo_TP           int            IDENTITY(1,1),
    Vida_Utill_Meses    int            NULL,
    Prefijo_codigo      char(15)       NULL,
    Descripcion         varchar(30)    NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY CLUSTERED (Codigo_TP)
)
go



IF OBJECT_ID('Tipo de activos') IS NOT NULL
    PRINT '<<< CREATED TABLE Tipo de activos >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Tipo de activos >>>'
go

/* 
 * TABLE: [Tipo de bajas] 
 */

CREATE TABLE [Tipo de bajas](
    Codigo         int            IDENTITY(1,1),
    Descripcion    varchar(30)    NULL,
    CONSTRAINT PK15 PRIMARY KEY CLUSTERED (Codigo)
)
go



IF OBJECT_ID('Tipo de bajas') IS NOT NULL
    PRINT '<<< CREATED TABLE Tipo de bajas >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Tipo de bajas >>>'
go

/* 
 * TABLE: [Tipo de Ingreso] 
 */

CREATE TABLE [Tipo de Ingreso](
    Codigo_TI      int            IDENTITY(1,1),
    Descripcion    varchar(20)    NULL,
    CONSTRAINT PK14 PRIMARY KEY CLUSTERED (Codigo_TI)
)
go



IF OBJECT_ID('Tipo de Ingreso') IS NOT NULL
    PRINT '<<< CREATED TABLE Tipo de Ingreso >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Tipo de Ingreso >>>'
go

/* 
 * TABLE: Traspasos 
 */

CREATE TABLE Traspasos(
    Codigo                int            IDENTITY(1,1),
    Codigo_Responsable    int            NOT NULL,
    Codigo_Ubicacion      int            NOT NULL,
    Codigo_activo         char(10)       NOT NULL,
    Fecha_Traslado        date           NULL,
    Observacion           varchar(50)    NULL,
    Motivo                varchar(30)    NULL,
    CONSTRAINT PK8 PRIMARY KEY CLUSTERED (Codigo, Codigo_Responsable, Codigo_Ubicacion, Codigo_activo)
)
go



IF OBJECT_ID('Traspasos') IS NOT NULL
    PRINT '<<< CREATED TABLE Traspasos >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Traspasos >>>'
go

/* 
 * TABLE: Ubicaciones 
 */

CREATE TABLE Ubicaciones(
    Codigo         int            NOT NULL,
    Descripcion    varchar(30)    NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY CLUSTERED (Codigo)
)
go



IF OBJECT_ID('Ubicaciones') IS NOT NULL
    PRINT '<<< CREATED TABLE Ubicaciones >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Ubicaciones >>>'
go

/* 
 * INDEX: Ref113 
 */

CREATE INDEX Ref113 ON Altas(Codigo_TP)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Altas') AND name='Ref113')
    PRINT '<<< CREATED INDEX Altas.Ref113 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Altas.Ref113 >>>'
go

/* 
 * INDEX: Ref1214 
 */

CREATE INDEX Ref1214 ON Altas(Codigo_Proveedores)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Altas') AND name='Ref1214')
    PRINT '<<< CREATED INDEX Altas.Ref1214 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Altas.Ref1214 >>>'
go

/* 
 * INDEX: Ref915 
 */

CREATE INDEX Ref915 ON Altas(Codigo_Responsable)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Altas') AND name='Ref915')
    PRINT '<<< CREATED INDEX Altas.Ref915 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Altas.Ref915 >>>'
go

/* 
 * INDEX: Ref1416 
 */

CREATE INDEX Ref1416 ON Altas(Codigo_TI)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Altas') AND name='Ref1416')
    PRINT '<<< CREATED INDEX Altas.Ref1416 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Altas.Ref1416 >>>'
go

/* 
 * INDEX: Ref1117 
 */

CREATE INDEX Ref1117 ON Altas(Codigo_Estado)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Altas') AND name='Ref1117')
    PRINT '<<< CREATED INDEX Altas.Ref1117 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Altas.Ref1117 >>>'
go

/* 
 * INDEX: Ref418 
 */

CREATE INDEX Ref418 ON Altas(Codigo_activo)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Altas') AND name='Ref418')
    PRINT '<<< CREATED INDEX Altas.Ref418 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Altas.Ref418 >>>'
go

/* 
 * INDEX: Ref227 
 */

CREATE INDEX Ref227 ON Altas([Codigo_Ubicacion'])
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Altas') AND name='Ref227')
    PRINT '<<< CREATED INDEX Altas.Ref227 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Altas.Ref227 >>>'
go

/* 
 * INDEX: Ref1522 
 */

CREATE INDEX Ref1522 ON Bajas([Tipo de baja])
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Bajas') AND name='Ref1522')
    PRINT '<<< CREATED INDEX Bajas.Ref1522 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Bajas.Ref1522 >>>'
go

/* 
 * INDEX: Ref223 
 */

CREATE INDEX Ref223 ON Bajas(Codigo_ubicacion)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Bajas') AND name='Ref223')
    PRINT '<<< CREATED INDEX Bajas.Ref223 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Bajas.Ref223 >>>'
go

/* 
 * INDEX: Ref924 
 */

CREATE INDEX Ref924 ON Bajas(Codigo_responsable)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Bajas') AND name='Ref924')
    PRINT '<<< CREATED INDEX Bajas.Ref924 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Bajas.Ref924 >>>'
go

/* 
 * INDEX: Ref425 
 */

CREATE INDEX Ref425 ON Bajas(Codigo_activo)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Bajas') AND name='Ref425')
    PRINT '<<< CREATED INDEX Bajas.Ref425 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Bajas.Ref425 >>>'
go

/* 
 * INDEX: Ref426 
 */

CREATE INDEX Ref426 ON Depreciaciones(Codigo_activo)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Depreciaciones') AND name='Ref426')
    PRINT '<<< CREATED INDEX Depreciaciones.Ref426 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Depreciaciones.Ref426 >>>'
go

/* 
 * INDEX: Ref919 
 */

CREATE INDEX Ref919 ON Traspasos(Codigo_Responsable)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Traspasos') AND name='Ref919')
    PRINT '<<< CREATED INDEX Traspasos.Ref919 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Traspasos.Ref919 >>>'
go

/* 
 * INDEX: Ref220 
 */

CREATE INDEX Ref220 ON Traspasos(Codigo_Ubicacion)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Traspasos') AND name='Ref220')
    PRINT '<<< CREATED INDEX Traspasos.Ref220 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Traspasos.Ref220 >>>'
go

/* 
 * INDEX: Ref421 
 */

CREATE INDEX Ref421 ON Traspasos(Codigo_activo)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Traspasos') AND name='Ref421')
    PRINT '<<< CREATED INDEX Traspasos.Ref421 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Traspasos.Ref421 >>>'
go

/* 
 * TABLE: Altas 
 */

ALTER TABLE Altas ADD CONSTRAINT RefTipo_de_activos131 
    FOREIGN KEY (Codigo_TP)
    REFERENCES [Tipo de activos](Codigo_TP)
go

ALTER TABLE Altas ADD CONSTRAINT RefProveedores141 
    FOREIGN KEY (Codigo_Proveedores)
    REFERENCES Proveedores(Codigo)
go

ALTER TABLE Altas ADD CONSTRAINT RefEmpleados151 
    FOREIGN KEY (Codigo_Responsable)
    REFERENCES Empleados(Codigo)
go

ALTER TABLE Altas ADD CONSTRAINT RefTipo_de_Ingreso161 
    FOREIGN KEY (Codigo_TI)
    REFERENCES [Tipo de Ingreso](Codigo_TI)
go

ALTER TABLE Altas ADD CONSTRAINT RefEstado171 
    FOREIGN KEY (Codigo_Estado)
    REFERENCES Estado(Codigo_Estado)
go

ALTER TABLE Altas ADD CONSTRAINT RefActivos181 
    FOREIGN KEY (Codigo_activo)
    REFERENCES Activos(Codigo)
go

ALTER TABLE Altas ADD CONSTRAINT RefUbicaciones271 
    FOREIGN KEY ([Codigo_Ubicacion'])
    REFERENCES Ubicaciones(Codigo)
go


/* 
 * TABLE: Bajas 
 */

ALTER TABLE Bajas ADD CONSTRAINT RefTipo_de_bajas221 
    FOREIGN KEY ([Tipo de baja])
    REFERENCES [Tipo de bajas](Codigo)
go

ALTER TABLE Bajas ADD CONSTRAINT RefUbicaciones231 
    FOREIGN KEY (Codigo_ubicacion)
    REFERENCES Ubicaciones(Codigo)
go

ALTER TABLE Bajas ADD CONSTRAINT RefEmpleados241 
    FOREIGN KEY (Codigo_responsable)
    REFERENCES Empleados(Codigo)
go

ALTER TABLE Bajas ADD CONSTRAINT RefActivos251 
    FOREIGN KEY (Codigo_activo)
    REFERENCES Activos(Codigo)
go


/* 
 * TABLE: Depreciaciones 
 */

ALTER TABLE Depreciaciones ADD CONSTRAINT RefActivos261 
    FOREIGN KEY (Codigo_activo)
    REFERENCES Activos(Codigo)
go


/* 
 * TABLE: Traspasos 
 */

ALTER TABLE Traspasos ADD CONSTRAINT RefEmpleados191 
    FOREIGN KEY (Codigo_Responsable)
    REFERENCES Empleados(Codigo)
go

ALTER TABLE Traspasos ADD CONSTRAINT RefUbicaciones201 
    FOREIGN KEY (Codigo_Ubicacion)
    REFERENCES Ubicaciones(Codigo)
go

ALTER TABLE Traspasos ADD CONSTRAINT RefActivos211 
    FOREIGN KEY (Codigo_activo)
    REFERENCES Activos(Codigo)
go


