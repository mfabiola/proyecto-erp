﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BootstrapMvcSample.Controllers;
using NavigationRoutes;

namespace BootstrapMvcSample
{
    public class ExampleLayoutsRouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.MapNavigationRoute<CatalogosController>("Catalogos", c => c.Index())
                .AddChildRoute<ActivosController>("Activo Fijo", c => c.Index())
                .AddChildRoute<TiposActivosController>("Tipos de activo", c => c.Index())
                .AddChildRoute<UbicacionesController>("Ubicaciones", c => c.Index())
                .AddChildRoute<ProveedoresController>("Proveedores", c => c.Index());


            //routes.MapNavigationRoute<HomeController>("Automatic Scaffolding", c => c.Index());

            routes.MapNavigationRoute<HomeController>("Movimientos", c => c.Movimientos(), "")
                .AddChildRoute<HomeController>("Altas", c => c.Altas(), "")
                .AddChildRoute<HomeController>("Bajas", c => c.Bajas(), "")
                .AddChildRoute<HomeController>("Traspasos", c => c.Traspasos(), "");
                


            
            //routes.MapNavigationRoute<ExampleLayoutsController>("Example Layouts", c => c.Starter())
            //      .AddChildRoute<ExampleLayoutsController>("Marketing", c => c.Marketing())
            //      .AddChildRoute<ExampleLayoutsController>("Fluid", c => c.Fluid())
            //      .AddChildRoute<ExampleLayoutsController>("Sign In", c => c.SignIn())
            //    ;
        }
    }
}
