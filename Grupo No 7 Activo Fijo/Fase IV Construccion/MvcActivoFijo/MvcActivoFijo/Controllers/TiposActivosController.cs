﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using MvcActivoFijo.Models;

namespace BootstrapMvcSample.Controllers
{
    public class TiposActivosController : BootstrapBaseController
    {
        private Activo_FijoEntities dbContext = new Activo_FijoEntities();
        
        //
        // GET: /TiposActivos/

        public ActionResult Index()
        {
            return View(dbContext.Tipo_de_activos.ToList());
        }

        //
        // GET: /TiposActivos/Details/5
        public ActionResult Details(int id)
        {
            var tipoactivo = dbContext.Tipo_de_activos.First(p => p.Codigo_TP.Equals(id));
            if (tipoactivo == null)
            {
                return HttpNotFound();
            }   
            return View(tipoactivo);
        }




        //
        // GET: /TiposActivos/Create

        public ActionResult Create()
        {
            return View(new Tipo_de_activos());
        }

        //
        // POST: /TiposActivos/Create

        [HttpPost]
        public ActionResult Create(Tipo_de_activos tipoactivo)
        {
            if (ModelState.IsValid)
            {
                dbContext.Tipo_de_activos.AddObject(tipoactivo);
                dbContext.SaveChanges();
                Success("La informacion fue guardada con exito!");
                return RedirectToAction("Index");
            }

            Error("Hubo un error al momento de crear el objeto.");
            return View(tipoactivo);
        }

        //
        // GET: /TiposActivos/Edit/5

        public ActionResult Edit(int id)
        {
            var tipoactivo = dbContext.Tipo_de_activos.First(p => p.Codigo_TP.Equals(id));
            if (tipoactivo == null)
            {
                return HttpNotFound();
            }

            return View(tipoactivo);

        }

        //
        // POST: /TiposActivos/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Tipo_de_activos tipoactivo)
        {
            if (ModelState.IsValid)
            {
                var _activo = dbContext.Tipo_de_activos.Single(p => p.Codigo_TP == id);

                _activo.Descripcion = tipoactivo.Descripcion;
                _activo.Prefijo_codigo = tipoactivo.Prefijo_codigo;
                _activo.Vida_Utill_Meses = tipoactivo.Vida_Utill_Meses;
                dbContext.SaveChanges();
                Success("La informacion se ha actualizado con exito!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de actualizar el objeto.");
            return View(tipoactivo);
         }

        //
        // GET: /TiposActivos/Delete/5

        public ActionResult Delete(int id)
        {
            var tipoactivo = dbContext.Tipo_de_activos.First(p => p.Codigo_TP.Equals(id));
            if (tipoactivo == null)
            {
                return HttpNotFound();
            }
            return View(tipoactivo);
        }

        //
        // POST: /TiposActivos/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, Tipo_de_activos tipoactivo)
        {

            // TODO: Add delete logic here
            var _tipoactivo = dbContext.Tipo_de_activos.First(p => p.Codigo_TP.Equals(id));
            if (ModelState.IsValid)
            {
                dbContext.DeleteObject(_tipoactivo);
                dbContext.SaveChanges();
                Success("El objeto fue borrado exitosamente!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de eliminar el objeto.");
            return View();
        }
    }
}
