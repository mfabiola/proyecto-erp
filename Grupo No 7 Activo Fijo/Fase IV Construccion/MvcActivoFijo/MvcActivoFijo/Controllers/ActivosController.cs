﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcActivoFijo.Models;

namespace BootstrapMvcSample.Controllers
{
    public class ActivosController : BootstrapBaseController
    {
        Activo_FijoEntities activofijoContext = new Activo_FijoEntities();
        
  
        //
        // GET: /Activos/

        public ActionResult Index()
        {
            return View(activofijoContext.Activos.ToList());
        }

        //
        // GET: /Activos/Details/5

        public ActionResult Details(int id)
        {
            var activo = activofijoContext.Activos.First(p => p.Codigo.Equals(id));
            if (activo == null)
            {
                return HttpNotFound();
            }
            
            return View(activo);
        }

        //
        // GET: /Activos/Create

        public ActionResult Create()
        {
            return View(new Activos());
        }

        //
        // POST: /Activos/Create

        [HttpPost]
        public ActionResult Create(Activos activo)
        {
         
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    activofijoContext.AddToActivos(activo);
                    activofijoContext.SaveChanges();
                    Success("La informacion fue guardada con exito!");
                    return RedirectToAction("Index");
                }
            
                Error("Hubo un error al momento de crear el objeto.");
                return View(activo);
        }

        //
        // GET: /Activos/Edit/5

        public ActionResult Edit(int id)
        {
            var activo = activofijoContext.Activos.First(p => p.Codigo.Equals(id));
            if (activo == null)
            {
                return HttpNotFound();
            }

            return View(activo);
            
        }

        //
        // POST: /Activos/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Activos activo)
        {
            if (ModelState.IsValid)
            {
                var _activo = activofijoContext.Activos.Single(p=>p.Codigo==id);

                _activo.Fecha_Compra = activo.Fecha_Compra;
                _activo.Nombre = activo.Nombre;
                _activo.Color = activo.Color;
                _activo.Marca = activo.Marca;
                _activo.Modelo = activo.Modelo;
                _activo.Serie = activo.Serie;
                activofijoContext.SaveChanges();
                Success("La informacion se ha actualizado con exito!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de actualizar el objeto.");
            return View(activo);
        }

        //
        // GET: /Activos/Delete/5

        public ActionResult Delete(int id)
        {
            var activo = activofijoContext.Activos.First(p=>p.Codigo.Equals(id));
            if (activo == null)
            {
                return HttpNotFound();
            }
            return View(activo);
        }

        //
        // POST: /Activos/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, Activos activo)
        {
            
                // TODO: Add delete logic here
                var _activo = activofijoContext.Activos.First(p => p.Codigo.Equals(id));
                if (ModelState.IsValid)
                {
                    activofijoContext.DeleteObject(_activo);
                    activofijoContext.SaveChanges();
                    Success("El objeto fue borrado exitosamente!");
                    return RedirectToAction("Index");
                }
                Error("Hubo un error al momento de eliminar el objeto.");
                return View(activo);
            
           
        }
    }
}
