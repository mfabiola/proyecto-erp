﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcActivoFijo.Models;

namespace BootstrapMvcSample.Controllers
{
    public class UbicacionesController : BootstrapBaseController
    {
        Activo_FijoEntities dbcontext = new Activo_FijoEntities();
        
        //
        // GET: /Ubicaciones/

        public ActionResult Index()
        {
            return View(dbcontext.Ubicaciones.ToList());
        }

        //
        // GET: /Ubicaciones/Details/5

        public ActionResult Details(int id)
        {
            var ubicaciones = dbcontext.Ubicaciones.First(p => p.Codigo.Equals(id));
            if (ubicaciones == null)
            {
                return HttpNotFound();
            }
            return View(ubicaciones);
        }

        //
        // GET: /Ubicaciones/Create

        public ActionResult Create()
        {
            return View(new Ubicaciones());
        }

        //
        // POST: /Ubicaciones/Create

        [HttpPost]
        public ActionResult Create(Ubicaciones ubicacion)
        {
            if (ModelState.IsValid)
            {
                dbcontext.Ubicaciones.AddObject(ubicacion);
                dbcontext.SaveChanges();
                Success("La informacion fue guardada con exito!");
                return RedirectToAction("Index");
            }

            Error("Hubo un error al momento de crear el objeto.");
            return View(ubicacion);
        }

        //
        // GET: /Ubicaciones/Edit/5

        public ActionResult Edit(int id)
        {
            var ubicacion = dbcontext.Ubicaciones.First(p => p.Codigo.Equals(id));
            if (ubicacion == null)
            {
                return HttpNotFound();
            }

            return View(ubicacion);

        }

        //
        // POST: /Ubicaciones/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Ubicaciones ubicacion)
        {
            if (ModelState.IsValid)
            {
                var _ubicacion = dbcontext.Ubicaciones.Single(p => p.Codigo == id);

                _ubicacion.Descripcion = ubicacion.Descripcion;
                dbcontext.SaveChanges();
                Success("La informacion se ha actualizado con exito!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de actualizar el objeto.");
            return View(ubicacion);
        }

        //
        // GET: /Ubicaciones/Delete/5

        public ActionResult Delete(int id)
        {
            var ubicacion = dbcontext.Ubicaciones.First(p => p.Codigo.Equals(id));
            if (ubicacion == null)
            {
                return HttpNotFound();
            }
            return View(ubicacion);
        }

        //
        // POST: /Ubicaciones/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, Ubicaciones ubicacion)
        {

            // TODO: Add delete logic here
            var _ubicacion = dbcontext.Ubicaciones.First(p => p.Codigo.Equals(id));
            if (ModelState.IsValid)
            {
                dbcontext.DeleteObject(_ubicacion);
                dbcontext.SaveChanges();
                Success("El objeto fue borrado exitosamente!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de eliminar el objeto.");
            return View(ubicacion);
        }
    }
}
