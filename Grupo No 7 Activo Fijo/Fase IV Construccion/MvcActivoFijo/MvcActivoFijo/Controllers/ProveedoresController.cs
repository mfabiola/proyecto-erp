﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcActivoFijo.Models;


namespace BootstrapMvcSample.Controllers
{
    public class ProveedoresController : BootstrapBaseController
    {
        Activo_FijoEntities dbcontext = new Activo_FijoEntities();
        //
        // GET: /Proveedores/

        public ActionResult Index()
        {
            return View(dbcontext.Proveedores.ToList());
        }

        //
        // GET: /Proveedores/Details/5

        public ActionResult Details(int id)
        {
            var proveedor = dbcontext.Proveedores.First(p => p.Codigo.Equals(id));
            if (proveedor == null)
            {
                return HttpNotFound();
            }   
            
            return View(proveedor);
        }

        //
        // GET: /Proveedores/Create

        public ActionResult Create()
        {
            return View(new Proveedores());
        }

        //
        // POST: /Proveedores/Create

        [HttpPost]
        public ActionResult Create(Proveedores proveedor)
        {
            if (ModelState.IsValid)
            {
                dbcontext.Proveedores.AddObject(proveedor);
                dbcontext.SaveChanges();
                Success("La informacion fue guardada con exito!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de crear el objeto.");
            return View(proveedor);
        }

        //
        // GET: /Proveedores/Edit/5

        public ActionResult Edit(int id)
        {
            var proveedor = dbcontext.Proveedores.First(p => p.Codigo.Equals(id));
            if (proveedor == null)
            {
                return HttpNotFound();
            }

            return View(proveedor);

        }

        //
        // POST: /Proveedores/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Proveedores proveedor)
        {
            if (ModelState.IsValid)
            {
                var _proveedor = dbcontext.Proveedores.Single(p => p.Codigo == id);

                _proveedor.Dscripcion = proveedor.Dscripcion;
                dbcontext.SaveChanges();
                Success("La informacion se ha actualizado con exito!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de actualizar el objeto.");
            return View(proveedor);
        }

        //
        // GET: /Proveedores/Delete/5

        public ActionResult Delete(int id)
        {
            var proveedor = dbcontext.Proveedores.First(p => p.Codigo.Equals(id));
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        //
        // POST: /Proveedores/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, Proveedores proveedor)
        {

            // TODO: Add delete logic here
            var _proveedor = dbcontext.Proveedores.First(p => p.Codigo.Equals(id));
            if (ModelState.IsValid)
            {
                dbcontext.DeleteObject(_proveedor);
                dbcontext.SaveChanges();
                Success("El objeto fue borrado exitosamente!");
                return RedirectToAction("Index");
            }
            Error("Hubo un error al momento de eliminar el objeto.");
            return View(proveedor);
        }
    }
}
