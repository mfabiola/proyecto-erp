USE [master]
GO
/****** Object:  Database [Activo Fijo]    Script Date: 05/10/2013 12:19:02 ******/
CREATE DATABASE [Activo Fijo] ON  PRIMARY 
( NAME = N'Activo Fijo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Activo Fijo.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Activo Fijo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Activo Fijo_log.LDF' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Activo Fijo] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Activo Fijo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Activo Fijo] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Activo Fijo] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Activo Fijo] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Activo Fijo] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Activo Fijo] SET ARITHABORT OFF
GO
ALTER DATABASE [Activo Fijo] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Activo Fijo] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Activo Fijo] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Activo Fijo] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Activo Fijo] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Activo Fijo] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Activo Fijo] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Activo Fijo] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Activo Fijo] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Activo Fijo] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Activo Fijo] SET  ENABLE_BROKER
GO
ALTER DATABASE [Activo Fijo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Activo Fijo] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Activo Fijo] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Activo Fijo] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Activo Fijo] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Activo Fijo] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Activo Fijo] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Activo Fijo] SET  READ_WRITE
GO
ALTER DATABASE [Activo Fijo] SET RECOVERY FULL
GO
ALTER DATABASE [Activo Fijo] SET  MULTI_USER
GO
ALTER DATABASE [Activo Fijo] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Activo Fijo] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Activo Fijo', N'ON'
GO
USE [Activo Fijo]
GO
/****** Object:  Table [dbo].[Tipo de Ingreso]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipo de Ingreso](
	[Codigo_TI] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](20) NULL,
 CONSTRAINT [PK14] PRIMARY KEY CLUSTERED 
(
	[Codigo_TI] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipo de bajas]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipo de bajas](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](30) NULL,
 CONSTRAINT [PK15] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipo de activos]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipo de activos](
	[Codigo_TP] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](30) NOT NULL,
	[Prefijo_codigo] [char](15) NULL,
	[Vida_Utill_Meses] [int] NULL,
 CONSTRAINT [PK1] PRIMARY KEY CLUSTERED 
(
	[Codigo_TP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tipo de activos] ON
INSERT [dbo].[Tipo de activos] ([Codigo_TP], [Descripcion], [Prefijo_codigo], [Vida_Utill_Meses]) VALUES (1, N'Edificaciones', N'EDI            ', 480)
INSERT [dbo].[Tipo de activos] ([Codigo_TP], [Descripcion], [Prefijo_codigo], [Vida_Utill_Meses]) VALUES (2, N'Equipos de Computacion', N'CO             ', 48)
INSERT [dbo].[Tipo de activos] ([Codigo_TP], [Descripcion], [Prefijo_codigo], [Vida_Utill_Meses]) VALUES (3, N'Instalaciones', N'INS            ', 96)
INSERT [dbo].[Tipo de activos] ([Codigo_TP], [Descripcion], [Prefijo_codigo], [Vida_Utill_Meses]) VALUES (4, N'Equipos de Comunicaciones', N'ECO            ', 96)
INSERT [dbo].[Tipo de activos] ([Codigo_TP], [Descripcion], [Prefijo_codigo], [Vida_Utill_Meses]) VALUES (5, N'Muebles y Equipo de viviendas', N'MYE            ', 60)
INSERT [dbo].[Tipo de activos] ([Codigo_TP], [Descripcion], [Prefijo_codigo], [Vida_Utill_Meses]) VALUES (6, N'Muebles y Equipo de Oficinas', N'MEO            ', 120)
INSERT [dbo].[Tipo de activos] ([Codigo_TP], [Descripcion], [Prefijo_codigo], [Vida_Utill_Meses]) VALUES (7, N'Herramientas', N'AAAA           ', 48)
SET IDENTITY_INSERT [dbo].[Tipo de activos] OFF
/****** Object:  Table [dbo].[Proveedores]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedores](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Dscripcion] [varchar](30) NOT NULL,
 CONSTRAINT [PK12] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Proveedores] ON
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (1, N'Mac Center')
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (2, N'DataTex')
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (3, N'Universal')
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (4, N'HP Managua')
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (5, N'COMTECH')
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (6, N'Android Store')
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (7, N'MAC 2')
INSERT [dbo].[Proveedores] ([Codigo], [Dscripcion]) VALUES (8, N'Electronic SA 2')
SET IDENTITY_INSERT [dbo].[Proveedores] OFF
/****** Object:  Table [dbo].[Estado]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Estado](
	[Codigo_Estado] [char](10) NOT NULL,
	[Descripcion] [varchar](20) NOT NULL,
 CONSTRAINT [PK11] PRIMARY KEY CLUSTERED 
(
	[Codigo_Estado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleados]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleados](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_Completo] [varchar](40) NOT NULL,
 CONSTRAINT [PK9] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Activos]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activos](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Fecha_Compra] [date] NULL,
	[Nombre] [varchar](30) NULL,
	[Color] [char](10) NULL,
	[Marca] [char](10) NULL,
	[Modelo] [char](15) NULL,
	[Serie] [char](15) NULL,
 CONSTRAINT [PK4] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Activos] ON
INSERT [dbo].[Activos] ([Codigo], [Fecha_Compra], [Nombre], [Color], [Marca], [Modelo], [Serie]) VALUES (1, CAST(0x2F350B00 AS Date), N'Computadora', N'Negra     ', N'Toshiba   ', N'101030-11      ', N'LTPX           ')
INSERT [dbo].[Activos] ([Codigo], [Fecha_Compra], [Nombre], [Color], [Marca], [Modelo], [Serie]) VALUES (2, CAST(0x4E350B00 AS Date), N'Escritorio', N'Cafe      ', N'Blandir   ', N'desk10X        ', N'DPX            ')
INSERT [dbo].[Activos] ([Codigo], [Fecha_Compra], [Nombre], [Color], [Marca], [Modelo], [Serie]) VALUES (3, CAST(0xB0370B00 AS Date), N'Lapicero', N'Negro     ', N'BIC       ', N'123            ', N'pa2            ')
INSERT [dbo].[Activos] ([Codigo], [Fecha_Compra], [Nombre], [Color], [Marca], [Modelo], [Serie]) VALUES (4, CAST(0x14370B00 AS Date), N'Impresora', N'Blanca    ', N'Dell      ', N'Injeck TAIL    ', N'Printer S2013  ')
SET IDENTITY_INSERT [dbo].[Activos] OFF
/****** Object:  Table [dbo].[Ubicaciones]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ubicaciones](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](30) NOT NULL,
 CONSTRAINT [PK2] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Ubicaciones] ON
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (1, N'Administracion')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (2, N'Recursos Humanos')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (3, N'GERENCIA')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (4, N'Oficina Ventas')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (5, N'Sala Entrenamiento')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (6, N'Secretaria')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (7, N'Oficina Tecnica')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (8, N'Oficina Computos')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (9, N'Laboratorios')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (10, N'Sala de Reuniones')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (11, N'Deposito')
INSERT [dbo].[Ubicaciones] ([Codigo], [Descripcion]) VALUES (12, N'Comedor')
SET IDENTITY_INSERT [dbo].[Ubicaciones] OFF
/****** Object:  Table [dbo].[Traspasos]    Script Date: 05/10/2013 12:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Traspasos](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Codigo_Responsable] [int] NOT NULL,
	[Codigo_Ubicacion] [int] NOT NULL,
	[Codigo_activo] [int] NOT NULL,
	[Fecha_Traslado] [date] NULL,
	[Observacion] [varchar](50) NULL,
	[Motivo] [varchar](30) NULL,
 CONSTRAINT [PK8] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[Codigo_Responsable] ASC,
	[Codigo_Ubicacion] ASC,
	[Codigo_activo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [Ref220] ON [dbo].[Traspasos] 
(
	[Codigo_Ubicacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref421] ON [dbo].[Traspasos] 
(
	[Codigo_activo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref919] ON [dbo].[Traspasos] 
(
	[Codigo_Responsable] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Depreciaciones]    Script Date: 05/10/2013 12:19:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Depreciaciones](
	[Codigo] [int] NOT NULL,
	[Codigo_activo] [int] NOT NULL,
	[Depreciacion_Mensual] [float] NULL,
	[Depreciacion_Diaria] [float] NULL,
	[Fecha_Depreciacion] [date] NULL,
	[Depreciacion_Anual] [float] NULL,
 CONSTRAINT [PK16] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[Codigo_activo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref426] ON [dbo].[Depreciaciones] 
(
	[Codigo_activo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bajas]    Script Date: 05/10/2013 12:19:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bajas](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Tipo_de_baja] [int] NOT NULL,
	[Codigo_ubicacion] [int] NOT NULL,
	[Codigo_responsable] [int] NOT NULL,
	[Codigo_activo] [int] NOT NULL,
	[Fecha_Baja] [date] NULL,
	[Observacion] [varchar](30) NULL,
 CONSTRAINT [PK7] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[Tipo_de_baja] ASC,
	[Codigo_ubicacion] ASC,
	[Codigo_responsable] ASC,
	[Codigo_activo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [Ref1522] ON [dbo].[Bajas] 
(
	[Tipo_de_baja] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref223] ON [dbo].[Bajas] 
(
	[Codigo_ubicacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref425] ON [dbo].[Bajas] 
(
	[Codigo_activo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref924] ON [dbo].[Bajas] 
(
	[Codigo_responsable] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Altas]    Script Date: 05/10/2013 12:19:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Altas](
	[Codigo] [int] NOT NULL,
	[Codigo_TP] [int] NOT NULL,
	[Codigo_Proveedores] [int] NOT NULL,
	[Codigo_Responsable] [int] NOT NULL,
	[Codigo_TI] [int] NOT NULL,
	[Codigo_Estado] [char](10) NOT NULL,
	[Codigo_activo] [int] NOT NULL,
	[Codigo_Ubicacion] [int] NOT NULL,
	[Valor_Rescate] [float] NULL,
	[Observacion] [varchar](50) NULL,
	[Fecha_Alta] [date] NULL,
	[Numero_Factura] [int] NULL,
	[Fecha_Inicio_Depreciacion] [date] NULL,
	[Vida Util] [int] NULL,
	[Valor_Compra] [float] NULL,
	[Valor_depreciacion] [float] NULL,
	[%anual] [float] NULL,
	[Codigo_Proveedor] [int] NULL,
 CONSTRAINT [PK6] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[Codigo_TP] ASC,
	[Codigo_Proveedores] ASC,
	[Codigo_Responsable] ASC,
	[Codigo_TI] ASC,
	[Codigo_Estado] ASC,
	[Codigo_activo] ASC,
	[Codigo_Ubicacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [Ref1117] ON [dbo].[Altas] 
(
	[Codigo_Estado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref113] ON [dbo].[Altas] 
(
	[Codigo_TP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref1214] ON [dbo].[Altas] 
(
	[Codigo_Proveedores] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref1416] ON [dbo].[Altas] 
(
	[Codigo_TI] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref227] ON [dbo].[Altas] 
(
	[Codigo_Ubicacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref418] ON [dbo].[Altas] 
(
	[Codigo_activo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Ref915] ON [dbo].[Altas] 
(
	[Codigo_Responsable] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  ForeignKey [RefActivos211]    Script Date: 05/10/2013 12:19:02 ******/
ALTER TABLE [dbo].[Traspasos]  WITH CHECK ADD  CONSTRAINT [RefActivos211] FOREIGN KEY([Codigo_activo])
REFERENCES [dbo].[Activos] ([Codigo])
GO
ALTER TABLE [dbo].[Traspasos] CHECK CONSTRAINT [RefActivos211]
GO
/****** Object:  ForeignKey [RefEmpleados191]    Script Date: 05/10/2013 12:19:02 ******/
ALTER TABLE [dbo].[Traspasos]  WITH CHECK ADD  CONSTRAINT [RefEmpleados191] FOREIGN KEY([Codigo_Responsable])
REFERENCES [dbo].[Empleados] ([Codigo])
GO
ALTER TABLE [dbo].[Traspasos] CHECK CONSTRAINT [RefEmpleados191]
GO
/****** Object:  ForeignKey [RefUbicaciones201]    Script Date: 05/10/2013 12:19:02 ******/
ALTER TABLE [dbo].[Traspasos]  WITH CHECK ADD  CONSTRAINT [RefUbicaciones201] FOREIGN KEY([Codigo_Ubicacion])
REFERENCES [dbo].[Ubicaciones] ([Codigo])
GO
ALTER TABLE [dbo].[Traspasos] CHECK CONSTRAINT [RefUbicaciones201]
GO
/****** Object:  ForeignKey [RefActivos261]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Depreciaciones]  WITH CHECK ADD  CONSTRAINT [RefActivos261] FOREIGN KEY([Codigo_activo])
REFERENCES [dbo].[Activos] ([Codigo])
GO
ALTER TABLE [dbo].[Depreciaciones] CHECK CONSTRAINT [RefActivos261]
GO
/****** Object:  ForeignKey [RefActivos251]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Bajas]  WITH CHECK ADD  CONSTRAINT [RefActivos251] FOREIGN KEY([Codigo_activo])
REFERENCES [dbo].[Activos] ([Codigo])
GO
ALTER TABLE [dbo].[Bajas] CHECK CONSTRAINT [RefActivos251]
GO
/****** Object:  ForeignKey [RefEmpleados241]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Bajas]  WITH CHECK ADD  CONSTRAINT [RefEmpleados241] FOREIGN KEY([Codigo_responsable])
REFERENCES [dbo].[Empleados] ([Codigo])
GO
ALTER TABLE [dbo].[Bajas] CHECK CONSTRAINT [RefEmpleados241]
GO
/****** Object:  ForeignKey [RefTipo_de_bajas221]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Bajas]  WITH CHECK ADD  CONSTRAINT [RefTipo_de_bajas221] FOREIGN KEY([Tipo_de_baja])
REFERENCES [dbo].[Tipo de bajas] ([Codigo])
GO
ALTER TABLE [dbo].[Bajas] CHECK CONSTRAINT [RefTipo_de_bajas221]
GO
/****** Object:  ForeignKey [RefUbicaciones231]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Bajas]  WITH CHECK ADD  CONSTRAINT [RefUbicaciones231] FOREIGN KEY([Codigo_ubicacion])
REFERENCES [dbo].[Ubicaciones] ([Codigo])
GO
ALTER TABLE [dbo].[Bajas] CHECK CONSTRAINT [RefUbicaciones231]
GO
/****** Object:  ForeignKey [RefActivos181]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Altas]  WITH CHECK ADD  CONSTRAINT [RefActivos181] FOREIGN KEY([Codigo_activo])
REFERENCES [dbo].[Activos] ([Codigo])
GO
ALTER TABLE [dbo].[Altas] CHECK CONSTRAINT [RefActivos181]
GO
/****** Object:  ForeignKey [RefEmpleados151]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Altas]  WITH CHECK ADD  CONSTRAINT [RefEmpleados151] FOREIGN KEY([Codigo_Responsable])
REFERENCES [dbo].[Empleados] ([Codigo])
GO
ALTER TABLE [dbo].[Altas] CHECK CONSTRAINT [RefEmpleados151]
GO
/****** Object:  ForeignKey [RefEstado171]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Altas]  WITH CHECK ADD  CONSTRAINT [RefEstado171] FOREIGN KEY([Codigo_Estado])
REFERENCES [dbo].[Estado] ([Codigo_Estado])
GO
ALTER TABLE [dbo].[Altas] CHECK CONSTRAINT [RefEstado171]
GO
/****** Object:  ForeignKey [RefProveedores141]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Altas]  WITH CHECK ADD  CONSTRAINT [RefProveedores141] FOREIGN KEY([Codigo_Proveedores])
REFERENCES [dbo].[Proveedores] ([Codigo])
GO
ALTER TABLE [dbo].[Altas] CHECK CONSTRAINT [RefProveedores141]
GO
/****** Object:  ForeignKey [RefTipo_de_activos131]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Altas]  WITH CHECK ADD  CONSTRAINT [RefTipo_de_activos131] FOREIGN KEY([Codigo_TP])
REFERENCES [dbo].[Tipo de activos] ([Codigo_TP])
GO
ALTER TABLE [dbo].[Altas] CHECK CONSTRAINT [RefTipo_de_activos131]
GO
/****** Object:  ForeignKey [RefTipo_de_Ingreso161]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Altas]  WITH CHECK ADD  CONSTRAINT [RefTipo_de_Ingreso161] FOREIGN KEY([Codigo_TI])
REFERENCES [dbo].[Tipo de Ingreso] ([Codigo_TI])
GO
ALTER TABLE [dbo].[Altas] CHECK CONSTRAINT [RefTipo_de_Ingreso161]
GO
/****** Object:  ForeignKey [RefUbicaciones271]    Script Date: 05/10/2013 12:19:03 ******/
ALTER TABLE [dbo].[Altas]  WITH CHECK ADD  CONSTRAINT [RefUbicaciones271] FOREIGN KEY([Codigo_Ubicacion])
REFERENCES [dbo].[Ubicaciones] ([Codigo])
GO
ALTER TABLE [dbo].[Altas] CHECK CONSTRAINT [RefUbicaciones271]
GO
