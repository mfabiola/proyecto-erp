/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      xxx
 * Project :      CuentasPorPagar-ModeloEntidadRelacion.dm1
 * Author :       personal
 *
 * Date Created : Saturday, May 04, 2013 15:43:44
 * Target DBMS : Microsoft SQL Server 2008
 */

/* 
 * TABLE: DetalleDePago 
 */

CREATE TABLE DetalleDePago(
    fecha          date                NOT NULL,
    monto          double precision    NOT NULL,
    idDocumento    int                 NOT NULL,
    idDeuda        int                 NOT NULL,
    idPago         int                 NOT NULL
)
go



IF OBJECT_ID('DetalleDePago') IS NOT NULL
    PRINT '<<< CREATED TABLE DetalleDePago >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DetalleDePago >>>'
go

/* 
 * TABLE: Deuda 
 */

CREATE TABLE Deuda(
    idDeuda          int                 IDENTITY(1,1),
    Detalle          text                NOT NULL,
    cantidadDeuda    double precision    NOT NULL,
    fechaFin         date                NOT NULL,
    fechaInicio      date                NOT NULL,
    idProveedor      int                 NOT NULL,
    idDocumento      int                 NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY NONCLUSTERED (idDeuda, idDocumento)
)
go



IF OBJECT_ID('Deuda') IS NOT NULL
    PRINT '<<< CREATED TABLE Deuda >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Deuda >>>'
go

/* 
 * TABLE: Documento 
 */

CREATE TABLE Documento(
    idDocumento        int     IDENTITY(1,1),
    URL                text    NOT NULL,
    idTipoDocumento    int     NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY NONCLUSTERED (idDocumento)
)
go



IF OBJECT_ID('Documento') IS NOT NULL
    PRINT '<<< CREATED TABLE Documento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Documento >>>'
go

/* 
 * TABLE: FormaDePago 
 */

CREATE TABLE FormaDePago(
    idFormaDePago    int            IDENTITY(1,1),
    descripcion      varchar(50)    NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (idFormaDePago)
)
go



IF OBJECT_ID('FormaDePago') IS NOT NULL
    PRINT '<<< CREATED TABLE FormaDePago >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE FormaDePago >>>'
go

/* 
 * TABLE: Pago 
 */

CREATE TABLE Pago(
    idPago           int                 IDENTITY(1,1),
    fecha            date                NOT NULL,
    monto            double precision    NOT NULL,
    detalle          char(10)            NULL,
    idFormaDePago    int                 NOT NULL,
    CONSTRAINT PK6 PRIMARY KEY NONCLUSTERED (idPago)
)
go



IF OBJECT_ID('Pago') IS NOT NULL
    PRINT '<<< CREATED TABLE Pago >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Pago >>>'
go

/* 
 * TABLE: Proveedor 
 */

CREATE TABLE Proveedor(
    idProveedor    int            IDENTITY(1,1),
    Direccion      varchar(50)    NULL,
    Nombre         varchar(50)    NOT NULL,
    telefono       varchar(30)    NULL,
    correo         varchar(60)    NULL,
    CONSTRAINT PK4 PRIMARY KEY NONCLUSTERED (idProveedor)
)
go



IF OBJECT_ID('Proveedor') IS NOT NULL
    PRINT '<<< CREATED TABLE Proveedor >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Proveedor >>>'
go

/* 
 * TABLE: tipoDocumento 
 */

CREATE TABLE tipoDocumento(
    idTipoDocumento    int            IDENTITY(1,1),
    nombre             varchar(50)    NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY NONCLUSTERED (idTipoDocumento)
)
go



IF OBJECT_ID('tipoDocumento') IS NOT NULL
    PRINT '<<< CREATED TABLE tipoDocumento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE tipoDocumento >>>'
go

/* 
 * TABLE: DetalleDePago 
 */

ALTER TABLE DetalleDePago ADD CONSTRAINT RefDocumento5 
    FOREIGN KEY (idDocumento)
    REFERENCES Documento(idDocumento)
go

ALTER TABLE DetalleDePago ADD CONSTRAINT RefDeuda7 
    FOREIGN KEY (idDeuda, idDocumento)
    REFERENCES Deuda(idDeuda, idDocumento)
go

ALTER TABLE DetalleDePago ADD CONSTRAINT RefPago9 
    FOREIGN KEY (idPago)
    REFERENCES Pago(idPago)
go


/* 
 * TABLE: Deuda 
 */

ALTER TABLE Deuda ADD CONSTRAINT RefProveedor8 
    FOREIGN KEY (idProveedor)
    REFERENCES Proveedor(idProveedor)
go

ALTER TABLE Deuda ADD CONSTRAINT RefDocumento12 
    FOREIGN KEY (idDocumento)
    REFERENCES Documento(idDocumento)
go


/* 
 * TABLE: Documento 
 */

ALTER TABLE Documento ADD CONSTRAINT ReftipoDocumento4 
    FOREIGN KEY (idTipoDocumento)
    REFERENCES tipoDocumento(idTipoDocumento)
go


/* 
 * TABLE: Pago 
 */

ALTER TABLE Pago ADD CONSTRAINT RefFormaDePago10 
    FOREIGN KEY (idFormaDePago)
    REFERENCES FormaDePago(idFormaDePago)
go


