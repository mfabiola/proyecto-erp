﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CuentasPorPagar.Models;

namespace CuentasPorPagar.Controllers
{
    public class ProveedorController : Controller
    {
        private CuentasPorPagarEntities db = new CuentasPorPagarEntities();

        //
        // GET: /Proveedor/

        public ActionResult Index()
        {
            return View(db.Proveedors.ToList());
        }

        //
        // GET: /Proveedor/Details/5

        public ActionResult Details(int id = 0)
        {
            Proveedor proveedor = db.Proveedors.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        //
        // GET: /Proveedor/Create

        public ActionResult Create() 
        {
    
            Proveedor proveedor = db.Proveedors.ToList().Last();
            int cantProveedores = db.Proveedors.ToList().Count;
            int idultimo = proveedor.idProveedor;
            int nuevoid = 0;
            if (idultimo == cantProveedores)
            {
                nuevoid = idultimo + 1;
            }
            else if (idultimo != cantProveedores && idultimo > cantProveedores)
            {
                nuevoid = idultimo + 1;
            }
            else if (idultimo != cantProveedores && idultimo < cantProveedores)
            {
                nuevoid = cantProveedores + 1;
            }

            //if (ExisteProveedor(nuevoid)) {
            //    nuevoid = nuevoid + 1;
            //}

            
            
            //Proveedores<Proveedor> = db.Proveedors.ToList();
            //Proveedor proveedor = db.Proveedors.ToList().Last();
            //int nuevoid = 0;
            
            //Proveedor proveedor = db.Proveedors.ToList().Max();
            //if (proveedor != null)
            //{
            //    nuevoid = proveedor.idProveedor + 1;
            //}
            //else {
            //    nuevoid = 1;
            //}

            //if (ExisteProveedor(nuevoid)) {
            //    nuevoid = nuevoid + 1;
            //}

            
            ViewBag.nuevoid = nuevoid;
            return View();
        }

        //
        // POST: /Proveedor/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Proveedor proveedor)
        {
            if (db.Proveedors.Find(proveedor.idProveedor) != null)
            {
                ViewData["Error"] = "El Id de Proveedor ya existe.";
                return View(proveedor);

            } else if (ModelState.IsValid)

            {
                db.Proveedors.Add(proveedor);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            
            return View(proveedor);
        }

        //
        // GET: /Proveedor/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Proveedor proveedor = db.Proveedors.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        //
        // POST: /Proveedor/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Proveedor proveedor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proveedor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proveedor);
        }

        //
        // GET: /Proveedor/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Proveedor proveedor = db.Proveedors.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        //
        // POST: /Proveedor/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Proveedor proveedor = db.Proveedors.Find(id);
            db.Proveedors.Remove(proveedor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        protected Boolean ExisteProveedor(int id) {
            if (db.Proveedors.Find(id) != null)
            {
                return true;
            }
            else
                return false;        
        }
    }
}