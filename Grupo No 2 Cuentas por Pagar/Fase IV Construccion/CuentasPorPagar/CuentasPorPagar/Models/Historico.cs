﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CuentasPorPagar.Models
{
    public class Historico
    {
        public String Detalle { get; set; }
        public DateTime Fecha { get; set; }
        public float Debe { get; set; }
        public float Haber { get; set; }
        public float Saldo { get; set; }
    }
}
